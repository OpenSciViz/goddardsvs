int init_world_map(file,out_buff)
char *file;
unsigned char *out_buff;
{
unsigned char in_scan[1024];
int i, j, k, n, m, recs=0, pos=0;
int fd, nb, sz=1024;
char finished=0;

   printf("init_world_map> reading world map texture file...\n");
   fd = open(file,0);

   while( ! finished )
   {
      nb = read(fd,in_scan,sz);
      if( nb < sz )
      { 
         finished = 1; 
         continue; 
      }
      bcopy(sz,in_scan,&out_buff[recs*sz]);
      recs++;
   }
   close(fd);
   return;
}

