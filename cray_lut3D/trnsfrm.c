/* this defines the view to world coordinate transformation;
   according to Goldstein's Eulerian angles def. vtow[][] is A~ on
   page 109, with psi == 0.0!
*/
#include "ray.h"
trnsfrm()
{
double phi, theta;

/* theta is the colatitude of the view direction vector */
    theta = acos( vwpnt[2] / 
            sqrt(vwpnt[0]*vwpnt[0] + vwpnt[1]*vwpnt[1] + vwpnt[2]*vwpnt[2]) );

/* if x & y coord. are zero use phi = pi/2.0 */
   if( vwpnt[0] == 0.0 && vwpnt[1] == 0.0 ) 
      phi = 3.141592654/2.0;
   else /* if x coord. is zero use phi = pi */
      phi = 3.141592654;

/* otherwise phi is azimuth of viewpoint + 90 deg.*/
    if( vwpnt[0] != 0.0 ) phi = atan( vwpnt[1] / vwpnt[0] ) + 3.141592654/2.0;

    vtow[0][0] = cos(phi);
    vtow[0][1] = -sin(phi)*cos(theta);
    vtow[0][2] = sin(phi)*sin(theta);
    vtow[1][0] = sin(phi);
    vtow[1][1] = cos(phi)*cos(theta);
    vtow[1][2] = -cos(phi)*sin(theta);
    vtow[2][0] = 0.0;
    vtow[2][1] = sin(theta);
    vtow[2][2] = cos(theta);
}

