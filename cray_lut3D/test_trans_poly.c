#include "ray.h"
int test_trans_poly()
/* one ball and three square mirrors */
{
/* make use of these globals:
global POLY *poly[N_POLYS];
global VERTEX_LIST *vert[N_VERTS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 0;
NP = 2;

init_poly_texture("claips:[aips.other]globe.pixmap",world_map);
/* init light source postion  */
lamp.x = 300.0;
lamp.y = 700.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 127;
diffuse.green = 127;
diffuse.blue = 127;

/* a gray surface */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 0.5;
   refl[0]->green_coeff = 0.5;
   refl[0]->blue_coeff = 0.5;
/* a yellow surface */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 1.0;
   refl[1]->red_coeff = 0.9;
   refl[1]->green_coeff = 0.9;
   refl[1]->blue_coeff = 0.1;
/* a magenta surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.1;
   refl[2]->blue_coeff = 0.9;
/* a cyan surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.1;
   refl[3]->green_coeff = 0.9;
   refl[3]->blue_coeff = 0.9;

   poly[0] = (POLY *) malloc(sizeof(POLY));
   poly[1] = (POLY *) malloc(sizeof(POLY));
/* since none of the polygons share vertices in this scene, need to allocate
   8 distinct vertices */
   for( i=0; i<8; i++ )
   {
      vert[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coord[i] = (TRIPLET *) malloc(sizeof(VERTEX_LIST));
   }

/* root polygon is bottom square: */
   poly[0]->id = 0;
   poly[0]->thick = 0.0;
   poly[0]->n_vert = 4;
   poly[0]->norm.x = 0;
   poly[0]->norm.y = 0;
   poly[0]->norm.z = 1;
   poly[0]->tran = NULL;
   poly[0]->refl = refl[1]; /* yellow */
   poly[0]->vertex = vert[0];
   vert[0]->coord = coord[0];
   coord[0]->x = -1.0;
   coord[0]->y = -0.5;
   coord[0]->z = -1.0;
   vert[0]->next = vert[1];
   vert[1]->coord = coord[1];
   coord[1]->x = 1.0;
   coord[1]->y = -0.5;
   coord[1]->z = -1.0;
   vert[1]->next = vert[2];
   vert[2]->coord = coord[2];
   coord[2]->x = 1.0;
   coord[2]->y = 0.5;
   coord[2]->z = -1.0;
   vert[2]->next = vert[3];
   vert[3]->coord = coord[3];
   coord[3]->x = -1.0;
   coord[3]->y = 0.5;
   coord[3]->z = -1.0;
   vert[3]->next = vert[0];

/* next polygon: */   
   poly[1]->id = 1;
   poly[1]->thick = 0.1;
   poly[1]->n_vert = 4;
   poly[1]->norm.x = 0;
   poly[1]->norm.y = 0;
   poly[1]->norm.z = 1;
   poly[1]->refl = refl[0]; /* gray */
   poly[1]->tran = (REFRACT *) malloc(sizeof(REFRACT));
   poly[1]->tran->refract_indx = 3.0;
   poly[1]->tran->red_coeff = 0.9;
   poly[1]->tran->green_coeff = 0.4;
   poly[1]->tran->blue_coeff = 0.6;
   poly[1]->vertex = vert[4];
   vert[4]->coord = coord[4];
   coord[4]->x = -0.5;
   coord[4]->y = -0.5;
   coord[4]->z = 0.0;
   vert[4]->next = vert[5];
   vert[5]->coord = coord[5];
   coord[5]->x = 0.5;
   coord[5]->y = -0.5;
   coord[5]->z = 0.0;
   vert[5]->next = vert[6];
   vert[6]->coord = coord[6];
   coord[6]->x = 0.5;
   coord[6]->y = 0.5;
   coord[6]->z = 0.0;
   vert[6]->next = vert[7];
   vert[7]->coord = coord[7];
   coord[7]->x = -0.5;
   coord[7]->y = 0.5;
   coord[7]->z = 0.0;
   vert[7]->next = vert[4];
}
