 
#include "ray.h"
#define MAX_DEPTH 3

raycast(prev_surf,parent_ray)
SURFACE *prev_surf; /* add this cluge to help prevent successive reflections
      			from selected surface types... */
RAY *parent_ray;
{
   float refl_dir[3], trns_dir[3], emin[3];
   SURFACE *surf, *raystrike();
   int status, optics(), reflect(), transmit();
   char shadow();
   float costh;

#ifdef DEBUG
printf("raycast> from pos: %f  %f  %f\n",parent_ray->base[0],
       parent_ray->base[1],parent_ray->base[2]);
#endif
   parent_ray->strike = NULL;
   parent_ray->refl = NULL;
   parent_ray->tran = NULL;

   surf = NULL;

   parent_ray->intens.red = 0.0;
   parent_ray->intens.green = 0.0;
   parent_ray->intens.blue = 0.0;

/* first check the current depth */
   if( parent_ray->depth > MAX_DEPTH ) return(0);

/* then see if this ray strikes anything */
   if( (surf = raystrike(parent_ray->base,parent_ray->dir)) != NULL )
/* we got something, oh my how exciting!!!! */
   {
/* don't allow two successive reflections off polgons, for now */
        if( prev_surf != NULL )	
      	{
      	   if( prev_surf->type == descr_poly && prev_surf->id < 3 &&
      		surf->type == descr_poly && surf->id < 3)
           {
      		free(surf);
      		parent_ray->strike = NULL;
      		return(0);
      	   }
/* also prevent reflections of the rectangles off the celestial sphere */
      	   if( surf->type == descr_poly && surf->id < 3 &&
      		prev_surf->type == descr_ball && prev_surf->id == 101)
           {
      		free(surf);
      		parent_ray->strike = NULL;
      		return(0);
      	   }
        }
	parent_ray->strike = surf;
/* check shadow ray */
 	parent_ray->strike->shadow = FALSE; /* shadow(&surf->pos); */

        if( surf->refl != NULL )
        {
	   reflect(parent_ray->dir,&surf->norm,refl_dir);
           parent_ray->refl = (RAY *) malloc(sizeof(RAY));
	   parent_ray->refl->depth = parent_ray->depth + 1;
	   parent_ray->refl->base[0] = surf->pos.x;
	   parent_ray->refl->base[1] = surf->pos.y;
	   parent_ray->refl->base[2] = surf->pos.z;
	   parent_ray->refl->dir[0] = refl_dir[0];
	   parent_ray->refl->dir[1] = refl_dir[1];
	   parent_ray->refl->dir[2] = refl_dir[2];
	   raycast(surf,parent_ray->refl);
        }

	if( surf->tran != NULL )
        {
	   parent_ray->tran = (RAY *) malloc(sizeof(RAY));
	   parent_ray->tran->depth = parent_ray->depth + 1;
	   status = transmit(surf,parent_ray->dir,trns_dir,emin);
           if( status < 0 ) getchar();
	   parent_ray->tran->dir[0] = trns_dir[0];
	   parent_ray->tran->dir[1] = trns_dir[1];
	   parent_ray->tran->dir[2] = trns_dir[2];
	   parent_ray->tran->base[0] = emin[0];
	   parent_ray->tran->base[1] = emin[1];
	   parent_ray->tran->base[2] = emin[2];
	   raycast(surf,parent_ray->tran);
	}
/* if/when we get here we can evaluate this ray's color/intenisty, using a 
   scheme based on the surface properties and the colors of the incident
   light */
        optics(parent_ray);
/* once the intens of this ray has been determined we can free its
   children */

	free(parent_ray->strike);
	if(parent_ray->refl != NULL) free(parent_ray->refl);
	if(parent_ray->tran != NULL) free(parent_ray->tran);
   }
/* we struck out and --> intens of the parent == 0 */
   return(0);
}

