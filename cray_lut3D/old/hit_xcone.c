#include "ray.h"
SURFACE *hit_cone(con,emin,ray)
CONE *con;
float emin[3], ray[3];
{
    SURFACE *hit;
    float cc, mag, cvec[3], strike[3], normal[3];
    float sa, sb, s; /* stretch factor */
    float a, b, c;

    hit = NULL;

    a = ray[1]*ray[1] + ray[2]*ray[2] - ray[0]*ray[0];
    if( rabs(a) < 0.0000001 ) 
      	return( hit );	/* otherwise we have something un-solvable */
        
    b = 2.0 * (emin[1]*ray[1] + emin[2]*ray[2] - emin[0]*ray[0]);
    c = emin[1]*emin[1] + emin[2]*emin[2] - emin[0]*emin[0];

/* stretch factor is nearest positive soln to quadratic equation above */
    sa = -b/a/2.0 - sqrt(b*b - 4.0*a*c)/2.0;
    sb = -b/a/2.0 + sqrt(b*b - 4.0*a*c)/2.0;
    if( (sa < 0) && (sb < 0) ) return( hit );
    if( (sa > 0)  && (sb < 0) ) s = sa;
    if( (sb > 0)  && (sa < 0) ) s = sb;
    if( (sa > 0) && (sb > 0) ) 
    {
        if( sa > sb )
	    s = sb;
        else
	    s = sa;
    }
    if( s > VISIBILITY ) return( hit );

    strike[0] = s * ray[0] + emin[0];
    strike[1] = s * ray[1] + emin[1];
    strike[2] = s * ray[2] + emin[2];
    cvec[0] = strike[0] - con->top->x;
    cvec[1] = strike[1] - con->top->y;
    cvec[2] = strike[2] - con->top->z;
    mag = sqrt(cvec[0]*cvec[0] + cvec[1]*cvec[1] + cvec[2]*cvec[2]);
    cvec[0] = cvec[0]/mag;
    cvec[1] = cvec[1]/mag;
    cvec[2] = cvec[2]/mag;
    cc = cvec[0]*con->axis->x + cvec[1]*con->axis->y + cvec[2]*con->axis->z;

/* if we get here we need to allocate a surface structure to return */
    hit = (SURFACE *) malloc(sizeof(SURFACE));
    hit->type = &descr_cone[0];
    hit->id = con->id;
    hit->pos.x = strike[0];
    hit->pos.y = strike[1];
    hit->pos.z = strike[2];
/* to calc the normal of the cone at strike, first define u-vec on surface
   of cone directed towards intersection */
/* normal on the outside of the cone: */
    normal[0] = cvec[0] - 2.0*cc*con->axis->x;
    normal[1] = cvec[1] - 2.0*cc*con->axis->y;
    normal[2] = cvec[2] - 2.0*cc*con->axis->z;
    if( (normal[0]*ray[0] + normal[1]*ray[1] + normal[2]*ray[2]) < 0.0 )
    {	/* surface normal & ray directions should be in opposite directions */
        hit->norm.x = normal[0];
        hit->norm.y = normal[1];
        hit->norm.z = normal[2];
    }
    else	/* normal on the inside of the cone: */
    {
        hit->norm.x = cvec[0] + 2.0*cc*con->axis->x;
        hit->norm.y = cvec[1] + 2.0*cc*con->axis->y;
        hit->norm.z = cvec[2] + 2.0*cc*con->axis->z;
    }
    mag = sqrt(hit->norm.x*hit->norm.x + hit->norm.y*hit->norm.y +
		hit->norm.z*hit->norm.z);
    hit->norm.x = hit->norm.x / mag;
    hit->norm.y = hit->norm.y / mag;
    hit->norm.z = hit->norm.z / mag;
    hit->refl = con->refl;
    hit->tran = con->tran;
    return( hit );
}
