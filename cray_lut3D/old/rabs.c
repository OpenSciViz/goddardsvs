float rabs(a)
float a;
{
   float b;

   if( a >= 0.0 ) 
      b = a;
   else
      b = -a;

   return( b ); 
}

