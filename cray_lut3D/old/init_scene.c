#include "ray.h"
int init_scene()
{
static VERTEX_LIST *vert;
static POLY *poly;
static REFLECT *refl;
static REFRACT *tran;
int i;

#ifdef DEBUG
printf("init_scene> COBE satellite polygons...\n");
#endif

/* init light source postion  */
lamp.x = 0.0;
lamp.y = 0.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is white, but not as bright */
diffuse.red = 125;
diffuse.green = 125;
diffuse.blue = 125;

/* an object made of polygons is accessed via its root polygon */
/* give all the polygons the same optical attributes */
refl = (REFLECT *) malloc(sizeof(REFLECT));
refl->spec_pwr = 16;
refl->red_coeff = 0.9;
refl->green_coeff = 0.1;
refl->blue_coeff = 0.1;
tran = NULL;

/* first poly object: */
p_objs[0].id = 0;
p_objs[0].n_poly = 1;
/* define its bounding surface as a sphere */
p_objs[0].bs = (BOUNDING_SURF *) malloc(sizeof(BOUNDING_SURF));
p_objs[0].bs->r = 5.0;
p_objs[0].bs->x = 0.0;
p_objs[0].bs->y = 0.0;
p_objs[0].bs->z = 0.0;
/* define the 'central' or root polygon, this is a triangle */
p_objs[0].root = (POLY *) malloc(sizeof(POLY));
for( i = 0; i < 8 ; i++ )
    p_objs[0].root->near[i] = NULL; /* no other polygons nearby */
p_objs[0].root->refl = refl;
p_objs[0].root->tran = tran;
p_objs[0].root->vertex = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
p_objs[0].root->id = 0;
p_objs[0].root->n_vert = 4;
/* normal of polygon: */
p_objs[0].root->norm.x = 0.0;
p_objs[0].root->norm.y = 0.0;
p_objs[0].root->norm.z = 1.0;
/* vertices */
vert = p_objs[0].root->vertex;
vert->x = 10.0;
vert->y = 0.0;
vert->z = 0.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next (2nd) vertex */ 
vert = vert->next;
vert->x = 0.0;
vert->y = 10.0;
vert->z = 0.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next (3rd) vertex */
vert = vert->next;
vert->x = -10.0;
vert->y = 0.0;
vert->z = 0.0;
vert->next = p_objs[0].root->vertex; /* circular linked list */
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next (4rth) vertex is the last */
vert = vert->next;
vert->x = 0.0;
vert->y = -10.0;
vert->z = 0.0;
vert->next = p_objs[0].root->vertex; /* circular linked list */

/* second poly object: */
p_objs[1].id = 1;
p_objs[1].n_poly = 4;
/* define its bounding surface as a sphere */
p_objs[1].bs = (BOUNDING_SURF *) malloc(sizeof(BOUNDING_SURF));
p_objs[1].bs->r = 30.0;
p_objs[1].bs->x = 0.0;
p_objs[1].bs->y = 0.0;
p_objs[1].bs->z = 0.0;
/* define the 'central' or root polygon, this is a triangle */
p_objs[1].root = (POLY *) malloc(sizeof(POLY));
for( i = 3; i < 8 ; i++ )
    p_objs[1].root->near[i] = NULL; /* only near[0] thru [2] should exist */
p_objs[1].root->refl = refl;
p_objs[1].root->tran = tran;
p_objs[1].root->vertex = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
p_objs[1].root->id = 0;
p_objs[1].root->n_vert = 3;
/* normal of polygon: */
p_objs[1].root->norm.x = 0.0;
p_objs[1].root->norm.y = 0.0;
p_objs[1].root->norm.z = 1.0;
/* vertices */
vert = p_objs[1].root->vertex;
vert->x = 20.0;
vert->y = 0.0;
vert->z = 0.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next (2nd) vertex */ 
vert = vert->next;
vert->x = 20.0*cos(0.66666*3.1415926);
vert->y = 20.0*sin(0.66666*3.1415926);
vert->z = 0.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next (3rd) vertex is the last */
vert = vert->next;
vert->x = 20.0*cos(0.66666*3.1415926);
vert->y = -20.0*sin(0.66666*3.1415926);
vert->z = 0.0;
vert->next = p_objs[1].root->vertex; /* circular linked list */

/* second polygon is a rectangle */
p_objs[1].root->near[0] = (POLY *) malloc(sizeof(POLY));
poly = p_objs[1].root->near[0];
poly->refl = refl;
poly->tran = tran;
poly->id = 1;
poly->n_vert = 4;
for( i = 0; i < 8 ; i++ )
      poly->near[i] = NULL;	/* this polygon is at the scene edge */
/* normal of polygon: */
poly->norm.x = 0.0;
poly->norm.y = 1.0;
poly->norm.z = 0.0;
/* vertices */
vert = p_objs[1].root->vertex; /* share first vertex with root polygon */
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
poly->vertex = vert;
/* next vertex */
vert = poly->vertex->next;
vert->x = 20.0;
vert->y = 0.0;
vert->z = 20.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next vertex */
vert = vert->next;
vert->x = 30.0;
vert->y = 0.0;
vert->z = 20.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next vertex */
vert = vert->next;
vert->x = 30.0;
vert->y = 0.0;
vert->z = 0.0;
vert->next = p_objs[1].root->vertex; /* last vertex is first */

/* third polygon is a rectangle */
p_objs[1].root->near[1] = (POLY *) malloc(sizeof(POLY));
poly = p_objs[1].root->near[1];
poly->refl = refl;
poly->tran = tran;
poly->id = 2;
poly->n_vert = 4;
for( i = 0; i < 8 ; i++ )
      poly->near[i] = NULL;	/* this polygon is at the scene edge */
/* normal of polygon: */
poly->norm.x = cos(3.14159260 / 6.0);
poly->norm.y = sin(3.14159260 / 6.0);
poly->norm.z = 0.0;
/* vertices */
vert = p_objs[1].root->vertex->next; /* share secones[0]d vertex with root polygon */
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
poly->vertex = vert;
/* next vertex */
vert = vert->next;
vert->x = 30.0*cos(0.66666*3.1415926);
vert->y = 30.0*sin(0.66666*3.1415926);
vert->z = 0.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next vertex */
vert = vert->next;
vert->x = 30.0*cos(0.66666*3.1415926);
vert->y = 30.0*sin(0.66666*3.1415926);
vert->z = 20.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next vertex */
vert = vert->next;
vert->x = 20.0*cos(0.66666*3.1415926);
vert->y = 20.0*sin(0.66666*3.1415926);
vert->z = 20.0;
vert->next = poly->vertex;

/* fourth & final polygon is a rectangle */
p_objs[1].root->near[2] = (POLY *) malloc(sizeof(POLY));
poly = p_objs[1].root->near[2];
poly->refl = refl;
poly->tran = tran;
poly->id = 3;
poly->n_vert = 4;
for( i = 0; i < 8 ; i++ )
      poly->near[i] = NULL;	/* this polygon is at the scene edge */
/* normal of polygon: */
poly->norm.x = cos(3.14159260 / 6.0);
poly->norm.y = -sin(3.14159260 / 6.0);
poly->norm.z = 0.0;
/* vertices */
vert = p_objs[1].root->vertex->next->next; /* share third vertex with root polygon */
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
poly->vertex = vert;
/* next vertex */
vert = vert->next;
vert->x = 30.0*cos(0.66666*3.1415926);
vert->y = -30.0*sin(0.66666*3.1415926);;
vert->z = 0.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next vertex */
vert = vert->next;
vert->x = 30.0*cos(0.66666*3.1415926);
vert->y = -30.0*sin(0.66666*3.1415926);
vert->z = 20.0;
vert->next = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
/* next vertex */
vert = vert->next;
vert->x = 20.0*cos(0.66666*3.1415926);
vert->y = -20.0*sin(0.66666*3.1415926);
vert->z = 20.0;
vert->next = poly->vertex;

/* balls[0] center & size */
balls[0].id = 0;
balls[0].r = 5.0;
balls[0].thick = 0.0;
balls[0].x = 0.0;
balls[0].y = 0.0;
balls[0].z = 5.0;
balls[0].tran = NULL;
balls[0].refl = (REFLECT *) malloc(sizeof(REFLECT));
balls[0].refl->spec_pwr = 20.0;
balls[0].refl->red_coeff = 0.1;
balls[0].refl->green_coeff = 0.9;
balls[0].refl->blue_coeff = 0.1;

/* cone */
cones[0].id = 1;
cones[0].thick = 0.0;	/* solid */
cones[0].h = 10.0;
cones[0].r = 5.0;
cones[0].phase = 0.0;
cones[0].top = (TRIPLET *) malloc(sizeof(TRIPLET));
cones[0].bot = (TRIPLET *) malloc(sizeof(TRIPLET));
cones[0].axis = (TRIPLET *) malloc(sizeof(TRIPLET));
cones[0].top->x = 0.0;
cones[0].top->y = 0.0;
cones[0].top->z = 0.0;
cones[0].bot->x = 0.0;
cones[0].bot->y = 0.0;
cones[0].bot->z = 10.0;
cones[0].axis->x = (cones[0].bot->x - cones[0].top->x) / cones[0].h;
cones[0].axis->y = (cones[0].bot->y - cones[0].top->y) / cones[0].h;
cones[0].axis->z = (cones[0].bot->z - cones[0].top->z) / cones[0].h;
cones[0].refl = (REFLECT *) malloc(sizeof(REFLECT));
cones[0].refl->red_coeff = 0.1;
cones[0].refl->green_coeff = 0.1;
cones[0].refl->blue_coeff = 0.9;
cones[0].tran = NULL;

/* cylinder */
cyls[0].id = 1;
cyls[0].thick = 0.0;	/* solid */
cyls[0].h = 10.0;
cyls[0].r_sqr = 5.0 * 5.0;
cyls[0].phase = 0.0;
cyls[0].top = (TRIPLET *) malloc(sizeof(TRIPLET));
cyls[0].bot = (TRIPLET *) malloc(sizeof(TRIPLET));
cyls[0].axis = (TRIPLET *) malloc(sizeof(TRIPLET));
cyls[0].top->x = -5.0;
cyls[0].top->y = 0.0;
cyls[0].top->z = 6.0;
cyls[0].bot->x = 5.0;
cyls[0].bot->y = 0.0;
cyls[0].bot->z = 6.0;
cyls[0].axis->x = (cyls[0].top->x - cyls[0].bot->x) / cyls[0].h;
cyls[0].axis->y = (cyls[0].top->y - cyls[0].bot->y) / cyls[0].h;
cyls[0].axis->z = (cyls[0].top->z - cyls[0].bot->z) / cyls[0].h;
cyls[0].bt = cyls[0].bot->x * cyls[0].top->x + cyls[0].bot->y * cyls[0].top->y +
      		cyls[0].bot->z * cyls[0].top->z; 
cyls[0].bb = cyls[0].bot->x * cyls[0].bot->x + cyls[0].bot->y * cyls[0].bot->y +
      		cyls[0].bot->z * cyls[0].bot->z; 
cyls[0].refl = (REFLECT *) malloc(sizeof(REFLECT));
cyls[0].refl->red_coeff = 0.1;
cyls[0].refl->green_coeff = 0.1;
cyls[0].refl->blue_coeff = 0.9;
cyls[0].tran = NULL;

return(0);
}
