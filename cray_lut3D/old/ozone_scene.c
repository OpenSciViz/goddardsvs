#include "ray.h"
int ozone_scene()
/* one ball and three square mirrors */
{
/* make use of these globals:
global POLY *poly[N_POLYS];
global VERTEX_LIST *vert[N_VERTS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 2;
NP = 3;

init_world_map("claips:[aips.other]globe.pixmap",world_map);
init_ozone_map("claips:[aips.other]ozone.dirbe",ozone_map);

/* init light source postion  */
lamp.x = 300.0;
lamp.y = 700.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 64;
diffuse.green = 64;
diffuse.blue = 64;

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = 0.5;
   refl[1]->green_coeff = 0.5;
   refl[1]->blue_coeff = 0.5;
/* a yellow surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.9;
   refl[2]->blue_coeff = 0.1;
/* a magenta surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.9;
   refl[3]->green_coeff = 0.1;
   refl[3]->blue_coeff = 0.9;
/* a cyan surface */
   refl[4] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[4]->spec_pwr = 1.0;
   refl[4]->red_coeff = 0.1;
   refl[4]->green_coeff = 0.9;
   refl[4]->blue_coeff = 0.9;

/* place the celestial sphere in the center */
   balls[0].r = 2.5;
   balls[0].id = 0;
   balls[0].thick = 0.1;
   balls[0].refl = NULL; /* refl[0]; */
   balls[0].tran = (REFRACT *) malloc(sizeof(REFRACT));
   balls[0].tran->refract_indx = 1.5;
   balls[0].tran->red_coeff = 0.5;
   balls[0].tran->green_coeff = 0.5;
   balls[0].tran->blue_coeff = 0.5;
   balls[0].texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   balls[0].texture->index = -1;
   balls[0].texture->red = 0;
   balls[0].texture->green = 0;
   balls[0].texture->blue = 0;
   balls[0].x = 0.0;
   balls[0].y = 0.0;
   balls[0].z = 0.0;

/* place the earth sphere inside the celestial sphere */
   balls[1].r = 2.0;
   balls[1].id = 1;
   balls[1].thick = 0;
   balls[1].refl = refl[0]; 
   balls[1].tran = NULL;
   balls[1].texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   balls[1].texture->index = -1;
   balls[1].texture->red = 0;
   balls[1].texture->green = 0;
   balls[1].texture->blue = 0;
   balls[1].x = 0.0;
   balls[1].y = 0.0;
   balls[1].z = 0.0;

   poly[0] = (POLY *) malloc(sizeof(POLY));
   poly[1] = (POLY *) malloc(sizeof(POLY));
   poly[2] = (POLY *) malloc(sizeof(POLY));
/* since none of the polygons share vertices in this scene, need to allocate
   12 distinct vertices */
   for( i=0; i<12; i++ )
   {
      vert[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coord[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   }

/* root polygon is bottom square: */
   poly[0]->id = 0;
   poly[0]->n_vert = 4;
   poly[0]->norm.x = 0;
   poly[0]->norm.y = 0;
   poly[0]->norm.z = 1;
   poly[0]->tran = NULL;
   poly[0]->texture = NULL;
   poly[0]->refl = refl[0];  
   poly[0]->vertex = vert[0];
   vert[0]->coord = coord[0];
   coord[0]->x = -4;
   coord[0]->y = -4;
   coord[0]->z = -3.0;
   vert[0]->next = vert[1];
   vert[1]->coord = coord[1];
   coord[1]->x = 5.5;
   coord[1]->y = -4;
   coord[1]->z = -3.0;
   vert[1]->next = vert[2];
   vert[2]->coord = coord[2];
   coord[2]->x = 5.5;
   coord[2]->y = 5.5;
   coord[2]->z = -3.0;
   vert[2]->next = vert[3];
   vert[3]->coord = coord[3];
   coord[3]->x = -4;
   coord[3]->y = 5.5;
   coord[3]->z = -3.0;
   vert[3]->next = vert[0];

/* next polygon: */   
   poly[1]->id = 1;
   poly[1]->n_vert = 4;
   poly[1]->norm.x = 0;
   poly[1]->norm.y = 1;
   poly[1]->norm.z = 0;
   poly[1]->tran = NULL;
   poly[1]->texture = NULL;
   poly[1]->refl = refl[0]; 
   poly[1]->vertex = vert[4];
   vert[4]->coord = coord[4];
   coord[4]->x = -3.75;
   coord[4]->y = -4.0;
   coord[4]->z = -2.5;
   vert[4]->next = vert[5];
   vert[5]->coord = coord[5];
   coord[5]->x = -3.75;
   coord[5]->y = -4.0;
   coord[5]->z = 5;
   vert[5]->next = vert[6];
   vert[6]->coord = coord[6];
   coord[6]->x = 5.5;
   coord[6]->y = -4.0;
   coord[6]->z = 5;
   vert[6]->next = vert[7];
   vert[7]->coord = coord[7];
   coord[7]->x = 5.5;
   coord[7]->y = -4.0;
   coord[7]->z = -2.5;
   vert[7]->next = vert[4];

/* next polygon: */   
   poly[2]->id = 2;
   poly[2]->n_vert = 4;
   poly[2]->norm.x = 1;
   poly[2]->norm.y = 0;
   poly[2]->norm.z = 0;
   poly[2]->tran = NULL;
   poly[2]->texture = NULL;
   poly[2]->refl = refl[0]; 
   poly[2]->vertex = vert[8];
   vert[8]->coord = coord[8];
   coord[8]->x = -4.0;
   coord[8]->y = -3.75;
   coord[8]->z = -2.5;
   vert[8]->next = vert[9];
   vert[9]->coord = coord[9];
   coord[9]->x = -4.0;
   coord[9]->y = 5.5;
   coord[9]->z = -2.5;
   vert[9]->next = vert[10];
   vert[10]->coord = coord[10];
   coord[10]->x = -4.0;
   coord[10]->y = 5.5;
   coord[10]->z = 5;
   vert[10]->next = vert[11];
   vert[11]->coord = coord[11];
   coord[11]->x = -4.0;
   coord[11]->y = -3.75;
   coord[11]->z = 5;
   vert[11]->next = vert[8];
}
