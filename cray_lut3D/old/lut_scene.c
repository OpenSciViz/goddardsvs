#include "ray.h"

int lut_scene(shift)
float shift;
/* one ball and three square mirrors */
{
/* make use of these globals:
global POLY *polys[N_POLYS];
global VERTEX_LIST *verts[N_VERTS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;
float tmp, v1[3], v2[3], v3[3];

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 1;
NP = 3;

/* init light source postion  */
lamp.x = 300.0;
lamp.y = 700.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 64;
diffuse.green = 64;
diffuse.blue = 64;

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = 0.5;
   refl[1]->green_coeff = 0.5;
   refl[1]->blue_coeff = 0.5;
/* a yellow surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.9;
   refl[2]->blue_coeff = 0.1;
/* a magenta surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.9;
   refl[3]->green_coeff = 0.1;
   refl[3]->blue_coeff = 0.9;
/* a cyan surface */
   refl[4] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[4]->spec_pwr = 1.0;
   refl[4]->red_coeff = 0.1;
   refl[4]->green_coeff = 0.9;
   refl[4]->blue_coeff = 0.9;

/* place the earth sphere */
   balls[0].r = 2.50;
   balls[0].id = 101;
   balls[0].thick = 0;
   balls[0].refl = NULL; 
   balls[0].tran = NULL;
   balls[0].texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   balls[0].texture->index = -1;
   balls[0].texture->red = 0;
   balls[0].texture->green = 0;
   balls[0].texture->blue = 0;
   balls[0].x = 0.0;
   balls[0].y = 0.0;
   balls[0].z = 0.0;

   polys[0] = (POLY *) malloc(sizeof(POLY));
   polys[1] = (POLY *) malloc(sizeof(POLY));
   polys[2] = (POLY *) malloc(sizeof(POLY));
/* since none of the polygons share vertices in this scene, need to allocate
   12 distinct vertices */
   for( i=0; i<12; i++ )
   {
      verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   }

/* root polygon is bottom square: */
   polys[0]->id = 0;
   polys[0]->n_vert = 4;
   polys[0]->tran = NULL;
   polys[0]->texture = NULL;
   polys[0]->refl = refl[0];  
   polys[0]->vertex = verts[0];
   verts[0]->coord = coords[0];
   coords[0]->x = -4;
   coords[0]->y = -4;
   coords[0]->z = -3.0;
   verts[0]->next = verts[1];
   verts[1]->coord = coords[1];
   coords[1]->x = 5.5;
   coords[1]->y = -4;
   coords[1]->z = -3.0;
   verts[1]->next = verts[2];
   verts[2]->coord = coords[2];
   coords[2]->x = 5.5;
   coords[2]->y = 5.5;
   coords[2]->z = -3.0;
   verts[2]->next = verts[3];
   verts[3]->coord = coords[3];
   coords[3]->x = -4;
   coords[3]->y = 5.5;
   coords[3]->z = -3.0;
   verts[3]->next = verts[0];
   v1[0] = coords[3]->x - coords[0]->x;
   v1[1] = coords[3]->y - coords[0]->y;
   v1[2] = coords[3]->z - coords[0]->z;
   v2[0] = coords[1]->x - coords[0]->x;
   v2[1] = coords[1]->y - coords[0]->y;
   v2[2] = coords[1]->z - coords[0]->z;
/* v2 x v1 right hand rule: */
   v3[0] = v2[1]*v1[2] - v2[2]*v1[1];
   v3[1] = v2[2]*v1[0] - v2[0]*v1[2];
   v3[2] = v2[0]*v1[1] - v2[1]*v1[0];
   tmp = sqrt( v3[0]*v3[0] + v3[1]*v3[1] + v3[2]*v3[2] );
   polys[0]->norm.x = v3[0] / tmp; /* 1; */
   polys[0]->norm.y = v3[1] / tmp; /* 0; */
   polys[0]->norm.z = v3[2] / tmp; /* 0; */

/* next polygon: */   
   polys[1]->id = 1;
   polys[1]->n_vert = 4;
   polys[1]->tran = NULL;
   polys[1]->texture = NULL;
   polys[1]->refl = refl[0]; 
   polys[1]->vertex = verts[4];
   verts[4]->coord = coords[4];
   coords[4]->x = -3.75;
   coords[4]->y = -4.0;
   coords[4]->z = -2.5;
   verts[4]->next = verts[5];
   verts[5]->coord = coords[5];
   coords[5]->x = -3.75;
   coords[5]->y = -4.0;
   coords[5]->z = 5;
   verts[5]->next = verts[6];
   verts[6]->coord = coords[6];
   coords[6]->x = 5.5;
   coords[6]->y = -4.0 - shift; /* -4.0; */
   coords[6]->z = 5;
   verts[6]->next = verts[7];
   verts[7]->coord = coords[7];
   coords[7]->x = 5.5;
   coords[7]->y = -4.0 - shift; /* -4.0; */
   coords[7]->z = -2.5;
   verts[7]->next = verts[4];
   v1[0] = coords[7]->x - coords[4]->x;
   v1[1] = coords[7]->y - coords[4]->y;
   v1[2] = coords[7]->z - coords[4]->z;
   v2[0] = coords[5]->x - coords[4]->x;
   v2[1] = coords[5]->y - coords[4]->y;
   v2[2] = coords[5]->z - coords[4]->z;
/* v2 x v1 right hand rule: */
   v3[0] = v2[1]*v1[2] - v2[2]*v1[1];
   v3[1] = v2[2]*v1[0] - v2[0]*v1[2];
   v3[2] = v2[0]*v1[1] - v2[1]*v1[0];
   tmp = sqrt( v3[0]*v3[0] + v3[1]*v3[1] + v3[2]*v3[2] );
   polys[1]->norm.x = v3[0] / tmp; /* 0; */
   polys[1]->norm.y = v3[1] / tmp; /* 1; */
   polys[1]->norm.z = v3[2] / tmp; /* 0; */

/* next polygon: */   
   polys[2]->id = 2;
   polys[2]->n_vert = 4;
   polys[2]->tran = NULL;
   polys[2]->texture = NULL;
   polys[2]->refl = refl[0]; 
   polys[2]->vertex = verts[8];
   verts[8]->coord = coords[8];
   coords[8]->x = -4.0;
   coords[8]->y = -3.75;
   coords[8]->z = -2.5;
   verts[8]->next = verts[9];
   verts[9]->coord = coords[9];
   coords[9]->x = -4.0 - shift; /* -4.0; */
   coords[9]->y = 5.5;
   coords[9]->z = -2.5;
   verts[9]->next = verts[10];
   verts[10]->coord = coords[10];
   coords[10]->x = -4.0 - shift; /* -4.0; */
   coords[10]->y = 5.5;
   coords[10]->z = 5;
   verts[10]->next = verts[11];
   verts[11]->coord = coords[11];
   coords[11]->x = -4.0;
   coords[11]->y = -3.75;
   coords[11]->z = 5;
   verts[11]->next = verts[8];
   v1[0] = coords[11]->x - coords[8]->x;
   v1[1] = coords[11]->y - coords[8]->y;
   v1[2] = coords[11]->z - coords[8]->z;
   v2[0] = coords[9]->x - coords[8]->x;
   v2[1] = coords[9]->y - coords[8]->y;
   v2[2] = coords[9]->z - coords[8]->z;
/* v2 x v1 right hand rule: */
   v3[0] = v2[1]*v1[2] - v2[2]*v1[1];
   v3[1] = v2[2]*v1[0] - v2[0]*v1[2];
   v3[2] = v2[0]*v1[1] - v2[1]*v1[0];
   tmp = sqrt( v3[0]*v3[0] + v3[1]*v3[1] + v3[2]*v3[2] );
   polys[2]->norm.x = v3[0] / tmp; /* 1; */
   polys[2]->norm.y = v3[1] / tmp; /* 0; */
   polys[2]->norm.z = v3[2] / tmp; /* 0; */
}
