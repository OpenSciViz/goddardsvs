.suffixes
.suffixes .exe .olb .obj .for .inc .c .h

.for.obj 
      $(FORT)/noopt/deb/ext/object=$(MMS$TARGET) $(MMS$SOURCE)

.c.obj
      $(CC)/noopt/deb/object=$(MMS$TARGET) $(MMS$SOURCE)


all : upx cray.exe
      ! cray.exe ready.

c_obj = main.obj,hit_ball.obj,hit_bound.obj,hit_cone.obj,hit_cylinder.obj,-
        hit_poly.obj,optics.obj,raycast.obj,raycolor.obj,-
        raystrike.obj,reflect.obj,shadow.obj,transmit.obj,trnsfrm.obj,-
        vec_trnsfm.obj, power.obj, -
        poly_texture.obj, ball_texture.obj, -
        init_rot.obj, init_world_map.obj, bcopy.obj, -
        lut_scene.obj, globe_scene.obj, align_qpix.obj

upx_obj = cacdb2:[xrhon.utils.upx]upx_xyaxis.obj,-
      	  cacdb2:[xrhon.utils.upx]upx_axisxy.obj,-
          cacdb2:[xrhon.utils.upx]upx_bit_table_set.obj,-
          cacdb2:[xrhon.utils.upx]upx_forward_cube.obj,-
      	  cacdb2:[xrhon.utils.upx]upx_incube.obj,-
          cacdb2:[xrhon.utils.upx]upx_pixel_number.obj,-
     	  cacdb2:[xrhon.utils.upx]upx_pixel_vector.obj

upx :  cacdb2:[xrhon.utils.upx]upxlib($(upx_obj))
      ! upxlib ready.

cray.exe : craylib(unfor_io,$(c_obj))
      $(LINK) $(LINKFLAGS) craylib/lib/inc=main,-
       cacdb2:[xrhon.utils.upx]upxlib/l

$(c_obj) : ray.h
