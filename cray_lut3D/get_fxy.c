#include "ray.h"

SURFACE *get_fxy(id,x,y)
int id; float x, y;
{
   SURFACE *surf;
   char *type = "fxy\n";
   int i, j, k;
   float mag;

   surf = (SURFACE *) malloc(sizeof(SURFACE));
   surf->texture = NULL;
   surf->tran = NULL;
   surf->refl = refl[3];
   surf->type = type;
   surf->id = id;
   surf->pos.x = x;
   surf->pos.y = y;

   i = fabs(x); j = fabs(y);
   k = (i % 512) * (1 + (j % 512));
   surf->pos.z = fxy_surf_ptr[k];
   surf->norm.z = 1.0;
   k = ((i+1) % 512) * (1 + (j % 512));
   surf->norm.x = fxy_surf_ptr[k] - surf->pos.z;
   k = (i % 512) * (1 + ((j+1) % 512));
   surf->norm.y = fxy_surf_ptr[k] - surf->pos.z;
   mag = sqrt( surf->norm.z * surf->norm.z +
      		surf->norm.x * surf->norm.x +
      		surf->norm.y * surf->norm.y );
   surf->norm.x = -surf->norm.x / mag;
   surf->norm.y = -surf->norm.y / mag;
   surf->norm.z = surf->norm.z / mag;

/*
   {
      surf->pos.z = 1.0 + sin(x)*sin(y);
      surf->norm.x = -cos(x)*sin(y);
      surf->norm.y = -sin(x)*cos(y);
      surf->norm.z = sqrt( 1.0 - (surf->norm.x)*(surf->norm.x) - 
      			(surf->norm.y)*(surf->norm.y) );
   }
*/
   return( surf );
}
