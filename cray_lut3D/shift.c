#include <stdio.h>    /*  shfttest.c  */
   /*  This tests the bit-shifting capability in C, building on David Hon's
       observations   */
#define IVALUE 0xFF   /*  8 ones to start with  */

  main()
  {
  int shift, copyl, copyr;
  copyl = copyr = IVALUE;
  printf(" Here is the starting value: %o \n", IVALUE);
  printf("  Enter the value to shift by(0 - 7): ");
  scanf("%d", &shift);
  copyl <<= shift;
  printf(" Here's the new value, left-shifted: %o\n", copyl);
  copyr >>= shift;
  printf(" And here is the result of a right shift; %o\n", copyr);
  }
  
