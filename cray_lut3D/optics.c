#include "ray.h"
optics(parent_ray)
RAY *parent_ray;
{
   float unit_vec[3], refl_dir[3], cos_theta, spec, mag;
   COLOR reflection, transmission, texture;
   float wght=1.0;
   int reflect();
   
   texture.red = 0.0;
   texture.green = 0.0;
   texture.blue = 0.0;

   transmission.red = 0.0;
   transmission.green = 0.0;
   transmission.blue = 0.0;

   reflection.red = 0.0;
   reflection.green = 0.0;
   reflection.blue = 0.0;

/* if the surface has a texture pattern, and it is ball.id = 1,
   assume that's all we are interested in for now */

   if( parent_ray->strike->texture != NULL )
   {
      texture.red = parent_ray->strike->texture->red;
      texture.green = parent_ray->strike->texture->green;
      texture.blue = parent_ray->strike->texture->blue;
      if( parent_ray->strike->type == descr_ball &&
          parent_ray->strike->id == 101 )
      {
         textel = parent_ray->strike->texture->index;
         parent_ray->intens.red = texture.red;
         parent_ray->intens.green = texture.green; 
         parent_ray->intens.blue = texture.blue;
         return(0);
      }
   }
/* apply the laws of reflection and refraction, using the surface info,
   incident light intensities, and some add hoc rules.
   first check pointers to see if there is anything incident on the surface */
   if( parent_ray->tran != NULL )
   {
	transmission.red = parent_ray->strike->tran->red_coeff *
				parent_ray->tran->intens.red;
	transmission.green = parent_ray->strike->tran->green_coeff *
				parent_ray->tran->intens.green;
	transmission.blue = parent_ray->strike->tran->blue_coeff *
				parent_ray->tran->intens.blue;
   }
/* total refl is more involved, always some diffuse stuff: */
/* for this application, since raycast did not allow two successive
   reflections off the polygons, and we are interested in the diffuse
   reflection from the polygons only if they are NOT reflecting
   the spheres... */
   if( parent_ray->refl != NULL )
   {
      if( parent_ray->strike->type == descr_poly && 
          parent_ray->refl->strike == NULL ) /* &&
      	  parent_ray->strike->id != 0 ) just for the nrao bitmap */
      
      {
         reflection.red = parent_ray->strike->refl->red_coeff *
				diffuse.red;
         reflection.green = parent_ray->strike->refl->green_coeff *
				diffuse.green;
         reflection.blue = parent_ray->strike->refl->blue_coeff *
   				diffuse.blue;
      }

/* if we're not in shadow, may have some 'saturated-white' specular stuff
   from the lamp, evaluate local direction to lamp, and if the reflection 
   ray is within 90 deg. add in the lamp */

      if( parent_ray->strike->shadow ) /* reduce diffuse reflection by some fract. */
      {
         reflection.red = 0.5 * reflection.red;
         reflection.green = 0.5 * reflection.green;
         reflection.blue = 0.5 * reflection.blue;
      }
      else if( parent_ray->strike->refl->spec_pwr >= 0.0 )
/* specular light source reflection too... */
      {
   	 unit_vec[0] = lamp.x - parent_ray->strike->pos.x;
   	 unit_vec[1] = lamp.y - parent_ray->strike->pos.y;
   	 unit_vec[2] = lamp.z - parent_ray->strike->pos.z;
   	 mag = unit_vec[0] * unit_vec[0] +
			unit_vec[1] * unit_vec[1] +
				unit_vec[2] * unit_vec[2];
   	 mag = sqrt(mag);
   	 unit_vec[0] = unit_vec[0] / mag;
   	 unit_vec[1] = unit_vec[1] / mag;
   	 unit_vec[2] = unit_vec[2] / mag;

	 if( parent_ray->refl != NULL )
	 {
   	    cos_theta = parent_ray->refl->dir[0] * unit_vec[0] +
			parent_ray->refl->dir[1] * unit_vec[1] +
			parent_ray->refl->dir[2] * unit_vec[2];
	 }
	 else /* must get reflection info which otherise not available */
	 {
	    reflect(parent_ray->dir,&parent_ray->strike->norm,refl_dir);
   	    cos_theta = refl_dir[0] * unit_vec[0] +
			refl_dir[1] * unit_vec[1] +
			refl_dir[2] * unit_vec[2];
	 }
   	 if( cos_theta > 0 )
         {
	    spec = (float) pow(cos_theta, 
                              parent_ray->strike->refl->spec_pwr);
/*	    spec = cos_theta; */
	    reflection.red = reflection.red + spec * lamp.intens.red;
	    reflection.green = reflection.green + spec * lamp.intens.green;
	    reflection.blue = reflection.blue + spec * lamp.intens.blue;
	 }
      } /* end if (!shadow) and specular refl. of light src. is desired */	
/*    if( parent_ray->strike->type != descr_ball ) 
 and the remaining specular contrib., if this is not a ball */
      {
	 reflection.red = reflection.red + 
				parent_ray->strike->refl->red_coeff *
					parent_ray->refl->intens.red;     
	 reflection.green = reflection.green + 
				parent_ray->strike->refl->green_coeff *
					parent_ray->refl->intens.green;   
	 reflection.blue = reflection.blue + 
				parent_ray->strike->refl->blue_coeff *
					parent_ray->refl->intens.blue;    
#ifdef OUTTABLE
   if( textel == -99 )	/* if the textel has not been set yet... */
      textel = -1;
#endif
      }
   }
/* we're now ready to set the net intensity. assuming each of the three
   contributions is normalized to the range 0 - 255, we need to perform
   a weighted sum. for now, we'll use equal weights: */

   parent_ray->intens.red = wght * (texture.red + reflection.red + 
      					transmission.red);
   parent_ray->intens.green = wght * (texture.green + reflection.green + 
      					transmission.green);
   parent_ray->intens.blue = wght * (texture.blue + reflection.blue + 
      					transmission.blue);

   return(0);
}

