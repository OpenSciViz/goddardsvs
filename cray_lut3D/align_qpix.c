#include "ray.h"
/* this defines the view to world coordinate transformation;
   according to Goldstein's Eulerian angles def. vtow[][] is A~ on
   page 109, with psi == 0.0 to get it transpose/inverse for the world
   to view coordinates transform: wtov. by analogy the transformation
   that takes the unit vector associated with the requested quadsphere
   pixel is constructed by using it for another vtow_p that is
   concatinated with wtov. the completed rotation matrix should be 
   the one that takes the specified q-sphere pixel's unit vector into 
   alignment with the real view vector, all other vector should undergo
   to same general rotation. the end result will be a rotated quad-sphere
   with the selected pixel along the line-of-sight.
*/
align_qpix(pixel)
int pixel;
{
double phi, theta;
float uvec[3], vtow_p[3][3];
short res=5;
int i, j, k;

/* theta is the colatitude of the direction vector */
    upx_pixel_vector_(&pixel,&res,uvec);
    theta = acos( uvec[2] / 
            sqrt(uvec[0]*uvec[0] + uvec[1]*uvec[1] + uvec[2]*uvec[2]) );

/* if x & y coord. are zero use phi = pi/2.0 */
   if( uvec[0] == 0.0 && uvec[1] == 0.0 ) 
      phi = 3.141592654/2.0;
   else /* if x coord. is zero use phi = pi */
      phi = 3.141592654;

/* otherwise phi is azimuth of viewpoint + 90 deg.*/
    if( uvec[0] != 0.0 ) phi = atan( uvec[1] / uvec[0] ) + 3.141592654/2.0;

    vtow_p[0][0] = cos(phi);
    vtow_p[0][1] = -sin(phi)*cos(theta);
    vtow_p[0][2] = sin(phi)*sin(theta);
    vtow_p[1][0] = sin(phi);
    vtow_p[1][1] = cos(phi)*cos(theta);
    vtow_p[1][2] = -cos(phi)*sin(theta);
    vtow_p[2][0] = 0.0;
    vtow_p[2][1] = sin(theta);
    vtow_p[2][2] = cos(theta);

/* multiply: transpose(vtow[][]) x vtow_p[][] to get the rotate[][] */

    for( i = 0; i < 3; i++ )
      for( j = 0; j < 3; j++ )
        for( k = 0; k < 3; k++ )
      	  rotate[i][j] = rotate[i][j] + vtow[k][i] * vtow_p[k][j];

}

