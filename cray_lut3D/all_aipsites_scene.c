#include "ray.h"

static float lat_lon[61][2] = {	{-32.48,  -68.52},
				{-33.55,  151.10},
				{-42.54,  147.18},
				{-23.33,  -46.39},
				{ 51.05, -114.05},
				{ 49.13, -123.06},
				{ 45.25,  -75.43},
				{ 39.55,  116.26},
				{ 31.06,  121.22},
				{ 55.43,   12.34},
				{ 50.44,    7.06},
				{ 60.08,   25.00},
				{ 43.18,    5.22},
				{ 48.52,    2.20},
				{ 47.30,   19.03},
				{ 18.56,   72.51},
				{ 32.05,   34.46},
				{ 44.30,   11.20},
				{ 36.39,  138.10},
				{ 31.53, -116.38},
				{ 53.13,    6.35},
				{-33.18,   26.32},
				{ 37.10,   -3.35},
				{ 59.15,   18.20},
				{ 57.25,   12.00},
				{ 55.57,   -3.13},
				{ 50.52,    0.16},
				{ 53.16,   -2.07},
				{ 34.44,  -86.35},
				{ 32.15, -110.57},
				{ 34.00, -118.15},
				{ 37.53, -122.17},
				{ 40.02, -105.16},
				{ 38.55,  -77.00},
				{ 29.40,  -82.20},
				{ 33.45,  -84.23},
				{ 19.42, -155.04},
				{ 42.02,  -93.39},
				{ 42.02,  -87.41},
				{ 39.10,  -86.31},
				{ 30.30,  -91.10},
				{ 42.23,  -72.31},
				{ 39.00,  -76.53},
				{ 44.53,  -68.43},
				{ 42.18,  -83.43},
				{ 45.00,  -93.15},
				{ 38.40,  -90.15},
				{ 35.55,  -79.04},
				{ 34.04, -106.55},
				{ 40.21,  -74.40},
				{ 40.56,  -73.08},
				{ 39.59,  -83.03},
				{ 35.14,  -97.27},
				{ 40.00,  -75.10},
				{ 40.26,  -80.00},
				{ 18.29,  -66.44},
				{ 30.18,  -97.47},
				{ 38.02,  -78.29},
				{ 47.35, -122.20},
				{ 43.04,  -89.22},
				{ 41.20, -105.38},
      				};

int all_aipsites_scene()
/* one ball and three square mirrors */
{
/* make use of these globals:
global POLY *polys[N_POLYS];
global VERTEX_LIST *verts[N_VERTS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;
float tmp;

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 1 + 61; /* Earth + x AIPS sites (max) as tiny spheres on Earth */
NP = 3;

init_world_map("claips:[aips.other]globe.pixmap",world_map);
init_nrao_map(nrao_map);

/* init light source postion  */
lamp.x = 300.0;
lamp.y = 700.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 64;
diffuse.green = 64;
diffuse.blue = 64;

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = 0.5;
   refl[1]->green_coeff = 0.5;
   refl[1]->blue_coeff = 0.5;
/* a yellow surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.9;
   refl[2]->blue_coeff = 0.1;
/* a magenta surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.9;
   refl[3]->green_coeff = 0.1;
   refl[3]->blue_coeff = 0.9;
/* a cyan surface */
   refl[4] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[4]->spec_pwr = 1.0;
   refl[4]->red_coeff = 0.1;
   refl[4]->green_coeff = 0.9;
   refl[4]->blue_coeff = 0.9;

/* place the earth sphere */
   balls[0].r = 2.50;
   balls[0].id = 0;
   balls[0].thick = 0;
   balls[0].refl = NULL; 
   balls[0].tran = NULL;
   balls[0].texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   balls[0].texture->index = -1;
   balls[0].texture->red = 0;
   balls[0].texture->green = 0;
   balls[0].texture->blue = 0;
   balls[0].x = 0.0;
   balls[0].y = 0.0;
   balls[0].z = 0.0;

   for( i = 1; i < NB; i++ )
   {  
      balls[i].r = 0.05 * balls[0].r;
      balls[i].id = i;
      balls[i].thick = 0;
      balls[i].refl = refl[4]; 
      balls[i].tran = NULL;
      balls[i].texture = NULL;
      balls[i].x = cos(lat_lon[i-1][0]/57.3) * cos(lat_lon[i-1][1]/57.3);
      balls[i].y = cos(lat_lon[i-1][0]/57.3) * sin(lat_lon[i-1][1]/57.3);
      balls[i].z = sin(lat_lon[i-1][0]/57.3);
/* move the center out beyond the surface of the earth and rotate 90 deg around
   the z axis */
      tmp = balls[i].x;
      balls[i].x = -1.05 * balls[0].r * balls[i].y;
      balls[i].y = 1.05 * balls[0].r * tmp;
      balls[i].z = 1.05 * balls[0].r * balls[i].z;
   }

   polys[0] = (POLY *) malloc(sizeof(POLY));
   polys[1] = (POLY *) malloc(sizeof(POLY));
   polys[2] = (POLY *) malloc(sizeof(POLY));
/* since none of the polygons share vertices in this scene, need to allocate
   12 distinct vertices */
   for( i=0; i<12; i++ )
   {
      verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   }

/* root polygon is bottom square: */
   polys[0]->id = 0;
   polys[0]->n_vert = 4;
   polys[0]->norm.x = 0;
   polys[0]->norm.y = 0;
   polys[0]->norm.z = 1;
   polys[0]->tran = NULL;
   polys[0]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[0]->texture->index = -1;
   polys[0]->texture->red = 0;
   polys[0]->texture->green = 0;
   polys[0]->texture->blue = 0;
   polys[0]->refl = refl[0];  
   polys[0]->vertex = verts[0];
   verts[0]->coord = coords[0];
   coords[0]->x = -4;
   coords[0]->y = -4;
   coords[0]->z = -3.0;
   verts[0]->next = verts[1];
   verts[1]->coord = coords[1];
   coords[1]->x = 5.5;
   coords[1]->y = -4;
   coords[1]->z = -3.0;
   verts[1]->next = verts[2];
   verts[2]->coord = coords[2];
   coords[2]->x = 5.5;
   coords[2]->y = 5.5;
   coords[2]->z = -3.0;
   verts[2]->next = verts[3];
   verts[3]->coord = coords[3];
   coords[3]->x = -4;
   coords[3]->y = 5.5;
   coords[3]->z = -3.0;
   verts[3]->next = verts[0];

/* next polygon: */   
   polys[1]->id = 1;
   polys[1]->n_vert = 4;
   polys[1]->norm.x = 0;
   polys[1]->norm.y = 1;
   polys[1]->norm.z = 0;
   polys[1]->tran = NULL;
   polys[1]->texture = NULL;
   polys[1]->refl = refl[0]; 
   polys[1]->vertex = verts[4];
   verts[4]->coord = coords[4];
   coords[4]->x = -3.75;
   coords[4]->y = -4.0;
   coords[4]->z = -2.5;
   verts[4]->next = verts[5];
   verts[5]->coord = coords[5];
   coords[5]->x = -3.75;
   coords[5]->y = -4.0;
   coords[5]->z = 5;
   verts[5]->next = verts[6];
   verts[6]->coord = coords[6];
   coords[6]->x = 5.5;
   coords[6]->y = -4.0;
   coords[6]->z = 5;
   verts[6]->next = verts[7];
   verts[7]->coord = coords[7];
   coords[7]->x = 5.5;
   coords[7]->y = -4.0;
   coords[7]->z = -2.5;
   verts[7]->next = verts[4];

/* next polygon: */   
   polys[2]->id = 2;
   polys[2]->n_vert = 4;
   polys[2]->norm.x = 1;
   polys[2]->norm.y = 0;
   polys[2]->norm.z = 0;
   polys[2]->tran = NULL;
   polys[2]->texture = NULL;
   polys[2]->refl = refl[0]; 
   polys[2]->vertex = verts[8];
   verts[8]->coord = coords[8];
   coords[8]->x = -4.0;
   coords[8]->y = -3.75;
   coords[8]->z = -2.5;
   verts[8]->next = verts[9];
   verts[9]->coord = coords[9];
   coords[9]->x = -4.0;
   coords[9]->y = 5.5;
   coords[9]->z = -2.5;
   verts[9]->next = verts[10];
   verts[10]->coord = coords[10];
   coords[10]->x = -4.0;
   coords[10]->y = 5.5;
   coords[10]->z = 5;
   verts[10]->next = verts[11];
   verts[11]->coord = coords[11];
   coords[11]->x = -4.0;
   coords[11]->y = -3.75;
   coords[11]->z = 5;
   verts[11]->next = verts[8];
}
