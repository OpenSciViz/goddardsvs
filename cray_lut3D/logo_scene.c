#include "ray.h"
int logo_scene()
/* one ball and three square mirrors */
{
/* make use of these globals:
global POLY *polys[N_POLYS];
global VERTEX_LIST *verts[N_VERTS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 0;
NP = 3;

/* 
init_world_map("claips:[aips.other]globe.pixmap",world_map);
init_world_map("claips:[aips.other]ozone.dirbe",ozone_map);
*/
init_sinusoid_map("claips:[aips.other]sinusoid_100m.pixmap",sinusoid_map[0]);
init_sinusoid_map("claips:[aips.other]sinusoid_60m.pixmap",sinusoid_map[1]);
init_sinusoid_map("claips:[aips.other]sinusoid_25m.pixmap",sinusoid_map[2]);
/* init_logo_map("claips:[aips.other]logo_630.pixmap",logo_map); */
init_logo_map("claips:[aips.other]logo_nasa.pixmap",logo_map);
init_nrao_map(nrao_map);

/* init light source postion  */
lamp.x = 300.0;
lamp.y = 700.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 64;
diffuse.green = 64;
diffuse.blue = 64;

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = 0.5;
   refl[1]->green_coeff = 0.5;
   refl[1]->blue_coeff = 0.5;
/* a yellow surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 1.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.9;
   refl[2]->blue_coeff = 0.1;
/* a magenta surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 1.0;
   refl[3]->red_coeff = 0.9;
   refl[3]->green_coeff = 0.1;
   refl[3]->blue_coeff = 0.9;
/* a cyan surface */
   refl[4] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[4]->spec_pwr = 1.0;
   refl[4]->red_coeff = 0.1;
   refl[4]->green_coeff = 0.9;
   refl[4]->blue_coeff = 0.9;

   for( i = 0; i < NP+2; i++ )
     polys[i] = (POLY *) malloc(sizeof(POLY));

/* since none of the polygons(squares) share vertices in this scene, need to allocate
   4*NP distinct vertices */
   for( i = 0; i < 4*(NP+2); i++ )
   {
      verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   }

/* root polygon is bottom square: */
   polys[0]->id = 0;
   polys[0]->n_vert = 4;
   polys[0]->norm.x = 0;
   polys[0]->norm.y = 0;
   polys[0]->norm.z = 1;
   polys[0]->tran = NULL;
   polys[0]->refl = refl[0]; /* gray */
   polys[0]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[0]->texture->index = -1;
   polys[0]->texture->red = 0;
   polys[0]->texture->green = 0;
   polys[0]->texture->blue = 0;
   polys[0]->vertex = verts[0];
   verts[0]->coord = coords[0];
   coords[0]->x = -4;
   coords[0]->y = -4;
   coords[0]->z = -3.0;
   verts[0]->next = verts[1];
   verts[1]->coord = coords[1];
   coords[1]->x = 5.5;
   coords[1]->y = -4;
   coords[1]->z = -3.0;
   verts[1]->next = verts[2];
   verts[2]->coord = coords[2];
   coords[2]->x = 5.5;
   coords[2]->y = 5.5;
   coords[2]->z = -3.0;
   verts[2]->next = verts[3];
   verts[3]->coord = coords[3];
   coords[3]->x = -4;
   coords[3]->y = 5.5;
   coords[3]->z = -3.0;
   verts[3]->next = verts[0];

/* next polygon: */   
   polys[1]->id = 1;
   polys[1]->n_vert = 4;
   polys[1]->norm.x = 0;
   polys[1]->norm.y = 1;
   polys[1]->norm.z = 0;
   polys[1]->tran = NULL;
   polys[1]->refl = refl[0]; /* gray */
   polys[1]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[1]->texture->index = -1;
   polys[1]->texture->red = 0;
   polys[1]->texture->green = 0;
   polys[1]->texture->blue = 0;
   polys[1]->vertex = verts[4];
   verts[4]->coord = coords[4];
   coords[4]->x = -3.75;
   coords[4]->y = -4.0;
   coords[4]->z = -2.5;
   verts[4]->next = verts[5];
   verts[5]->coord = coords[5];
   coords[5]->x = -3.75;
   coords[5]->y = -4.0;
   coords[5]->z = 5;
   verts[5]->next = verts[6];
   verts[6]->coord = coords[6];
   coords[6]->x = 5.5;
   coords[6]->y = -4.0;
   coords[6]->z = 5;
   verts[6]->next = verts[7];
   verts[7]->coord = coords[7];
   coords[7]->x = 5.5;
   coords[7]->y = -4.0;
   coords[7]->z = -2.5;
   verts[7]->next = verts[4];

/* next polygon: */   
   polys[2]->id = 2;
   polys[2]->n_vert = 4;
   polys[2]->norm.x = 1;
   polys[2]->norm.y = 0;
   polys[2]->norm.z = 0;
   polys[2]->tran = NULL;
   polys[2]->refl = refl[0]; /* gray */
   polys[2]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[2]->texture->index = -1;
   polys[2]->texture->red = 0;
   polys[2]->texture->green = 0;
   polys[2]->texture->blue = 0;
   polys[2]->vertex = verts[8];
   verts[8]->coord = coords[8];
   coords[8]->x = -4.0;
   coords[8]->y = -3.75;
   coords[8]->z = -2.5;
   verts[8]->next = verts[9];
   verts[9]->coord = coords[9];
   coords[9]->x = -4.0;
   coords[9]->y = 5.5;
   coords[9]->z = -2.5;
   verts[9]->next = verts[10];
   verts[10]->coord = coords[10];
   coords[10]->x = -4.0;
   coords[10]->y = 5.5;
   coords[10]->z = 5;
   verts[10]->next = verts[11];
   verts[11]->coord = coords[11];
   coords[11]->x = -4.0;
   coords[11]->y = -3.75;
   coords[11]->z = 5;
   verts[11]->next = verts[8];

/* this guy is just above the bottom square and should show ozone: */
   polys[3]->id = 3;
   polys[3]->n_vert = 4;
   polys[3]->norm.x = 0;
   polys[3]->norm.y = 0;
   polys[3]->norm.z = 1;
   polys[3]->tran = (REFRACT *) malloc(sizeof(REFRACT));
   polys[3]->tran->refract_indx = 1.5;
   polys[3]->tran->red_coeff = .5;
   polys[3]->tran->green_coeff = .5;
   polys[3]->tran->blue_coeff = .5;
   polys[3]->refl = NULL;
   polys[3]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[3]->texture->index = -1;
   polys[3]->texture->red = 0;
   polys[3]->texture->green = 0;
   polys[3]->texture->blue = 0;
   polys[3]->vertex = verts[12];
   verts[12]->coord = coords[12];
   coords[12]->x = -4;
   coords[12]->y = -4;
   coords[12]->z = 1.;
   verts[12]->next = verts[13];
   verts[13]->coord = coords[13];
   coords[13]->x = 5.5;
   coords[13]->y = -4;
   coords[13]->z = 1.;
   verts[13]->next = verts[14];
   verts[14]->coord = coords[14];
   coords[14]->x = 5.5;
   coords[14]->y = 5.5;
   coords[14]->z = 1.;
   verts[14]->next = verts[15];
   verts[15]->coord = coords[15];
   coords[15]->x = -4;
   coords[15]->y = 5.5;
   coords[15]->z = 1.;
   verts[15]->next = verts[12];

/* and this guy is just above the previous square and should show a skymap: */
   polys[4]->id = 4;
   polys[4]->n_vert = 4;
   polys[4]->norm.x = 0;
   polys[4]->norm.y = 0;
   polys[4]->norm.z = 1;
   polys[4]->tran = (REFRACT *) malloc(sizeof(REFRACT));
   polys[4]->tran->refract_indx = 1.5;
   polys[4]->tran->red_coeff = .5;
   polys[4]->tran->green_coeff = .5;
   polys[4]->tran->blue_coeff = .5;
   polys[4]->refl = NULL; 
   polys[4]->texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   polys[4]->texture->index = -1;
   polys[4]->texture->red = 0;
   polys[4]->texture->green = 0;
   polys[4]->texture->blue = 0;
   polys[4]->vertex = verts[16];
   verts[16]->coord = coords[16];
   coords[16]->x = -4;
   coords[16]->y = -4;
   coords[16]->z = 5.;
   verts[16]->next = verts[17];
   verts[17]->coord = coords[17];
   coords[17]->x = 5.5;
   coords[17]->y = -4;
   coords[17]->z = 5.;
   verts[17]->next = verts[18];
   verts[18]->coord = coords[18];
   coords[18]->x = 5.5;
   coords[18]->y = 5.5;
   coords[18]->z = 5.;
   verts[18]->next = verts[19];
   verts[19]->coord = coords[19];
   coords[19]->x = -4;
   coords[19]->y = 5.5;
   coords[19]->z = 5.;
   verts[19]->next = verts[16];

}
