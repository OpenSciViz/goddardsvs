pro sar_hamm,infile,nsec 
; some useful constants
PI=3.1415926
C=300000000.0
SWATH_WIDTH = C*2.0/1000000.0/sqrt(2.0)
pb=450000000.0
v=2000.0
r0 = sqrt(2.0*10000.0^2)
dop_frq_rt = -2.0*pb*v*v/r0/c/(4.0*PI)
;
a0=assoc(1,fltarr(1024))
a1=assoc(1,fltarr(nsec*1024),4*1024)
a2=assoc(1,fltarr(1024,nsec*1024),(1+nsec)*4*1024)
openr,1,infile
; read in range ref. signal and perform range compression:
rr=a0(0)
; read in doppler ref. signal and perform azimuth compression:
ra=a1(0)
; try redefining the az. ref:
ra = cos(2*PI*dop_frq_rt*((indgen(nsec*1024)-nsec*512)*0.001)^2)
;for i=0, nsec-1 do ra(i)=a1(i+1)
; or try defining an azimuth ref. for each column (during azim. compression loop)
; read in raw data:
s=a2(0)
close,1
; define hamming window:
a_hamm = 0.231
b_hamm = 1 - 2.0*a_hamm
rn_hamm=2.0*a_hamm*cos(3.1415926/512.0*(indgen(1024)-512)) + b_hamm
az_hamm=2.0*a_hamm*cos(3.1415926/(nsec*512.0)*(indgen(nsec*1024)-nsec*512)) + b_hamm
fr=conj(fft(rn_hamm*rr,-1))
fa = conj(fft(az_hamm*ra,-1))
cs=complexarr(1024,nsec*1024)
shufr=cs
img=cs
;subimg=fltarr(1024,1024)
;idx=2*indgen(1024)
;subimg=s(*,idx)
window,0,xs=1024,ys=1024,ret=2
;tvscl,subimg
tvscl,s
empty
print,'doing range compression..."
for i=0,nsec*1024-1 do begin 
  fs=fft(s(*,i),-1)
  cs(*,i)=fft(fr*fs,1)
endfor
for i=0,nsec*1024-1 do begin 
  shufr(512:1023,i)=cs(0:511,i)
  shufr(0:511,i)=cs(512:1023,i)
endfor
tvscl,abs(shufr)^2
empty
print,'doing azimuth compression..."
shufra=cs
cs=transpose(shufra)
for i=0, 1023 do begin
  r0 = sqrt(10000.0^2 + (10000.0 + (i-512)/1024.0*SWATH_WIDTH)^2)
  dop_frq_rt = -2.0*pb*v*v/r0/c/(4.0*PI)
  ra = cos(2*3.1415926*dop_frq_rt*((indgen(nsec*1024)-nsec*512)*0.001)^2)
  fa = conj(fft(az_hamm*ra,-1))
  fs = fft(cs(*,i),-1)
  img(*,i) = fft(fa*fs,1)
;  subimg(*,i) = abs(img(idx,i))
endfor
;
for i=512,1023 do begin 
  shufra(512:1023,i)=img(0:511,i-512) 
  shufra(0:511,i)=img(512:1023,i-512)
endfor
for i=0,511 do begin 
  shufra(0:511,i)=img(512:1023,i+512) 
  shufra(512:1023,i)=img(0:511,i+512)
endfor
img=transpose(shufra)
print,'finished azimuth compression; enter ".con" to continue...'
stop
tvscl,abs(img)^2
empty
;
;tmp=transpose(subimg)
;for i=512,1023 do begin 
;  subimg(512:1023,i)=tmp(0:511,i-512) 
;  subimg(0:511,i)=tmp(512:1023,i-512)
;endfor
;for i=0,511 do begin 
;  subimg(0:511,i)=tmp(512:1023,i+512) 
;  subimg(512:1023,i)=tmp(0:511,i+512)
;endfor
;
;tvscl,abs(subimg)
;empty
stop
return
end

