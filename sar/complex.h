typedef struct FCOMPLEX {float r,i;} fcomplex;

extern fcomplex Cadd();
extern fcomplex Csub();
extern fcomplex Cmul();
extern fcomplex Complex();
extern fcomplex Conjg();
extern fcomplex Cdiv();
extern float Cabs();
extern fcomplex Csqrt();
extern fcomplex RCmul();

/* added by dh */
extern fcomplex Cexp();
extern float Camp();
