/*      program synthesize

 C*******************************************************************************
 C
 C     PROGRAM SYNTHESIZE, VERSION 3.0
 C
 C     This program was originally written by Fred Burnette of Jet Propulsion 
 C     Laboratory in March 1990.  The code was written in FORTRAN-77.
 C     The following program is "C" version of the original SYNTHESIZE program.
 C
 C     Program to read JPL compressed multipolarization SAR data and
 C     synthesize 8-bit images at different user-specified polarization
 C     combinations.  
 C
 C     Input to this program should be one JPL multi-polarization compressed
 C     data file.  The input file must have a uniform record size (width) of  C     10240 bytes (1024 pixels per line). 
 C
 C     The user then specifies a polarization combination by entering the 
 C     transmit and receive ellipticity and orientation angles in degrees.
 C     
 C     Two output images are generated (one with float data type and the other 
 C     with byte data type) containing the synthesized image of the area.
 C
 C
 C*******************************************************************************
*/


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#define PI 3.141592654
#define STOKES(x)    ( ((x) < 0) ?  ((-1.0)*(x)*(x)*(stokes[0][0])/(16129.0)) : ((x)*(x)*(stokes[0][0])/(16129.0)) )    
#define ROUND(y)     ( (int) ((y) + 0.5) )
#define MIN(a,b)     ( ((a) < (b)) ? (a) : (b) )
#define MAX(a,b)     ( ((a) > (b)) ? (a) : (b) )
/* #define OFFSET 100           */
/* #define NLINE (1279-OFFSET)  */
/* #define NHEAD 3	*/
/* #define NLINE 1279	*/
#define NHEAD 2
#define NLINE 750

int ireclen;                          /* BYTES PER RECORD */
int nhead;                            /* NUMBER OF HEADER LINES */
int nsamp;                            /* NUMBER OF SAMPLES PER RECORD */
int nline;                            /* NUMBER OF LINES IN IMAGE */
int nbytesamp;                        /* BYTES PER SAMPLE */
int ibytesoldhead;                    /* BYTE OFFSET TO OLD HEADER */
int ibytesuserhead;                   /* BYTE OFFSET TO USER HEADER */
int ibytesdata;                       /* BYTE OFFSET TO DATA */
float version;                        /* PROCESSOR VERSION NUMBER */
float rspace;                         /* RANGE PIXEL SPACING */
float azspace;                        /* AZIMUTH PIXEL SPACING */
static char projection[21];           /* SLANT/GROUND RANGE PROJECTION */
static char datatype1[10];             /* DATA TYPE */
static char datatype2[10];             /* DATA TYPE */

float rangenear;                      /* NEAR RANGE DISTANCE IN METERS */
float rangepix;                       /* RANGE PIXEL SPACING IN METERS */
float azpix;                          /* AZIMUTH PIXEL SPACING IN METERS */
float wav;                            /* RADAR WAVELENGTH IN METERS */
float drift;                          /* DRIFT ANGLE IN DEGREES */ 
float track;                          /* AIRCRAFT TRACK ANGLE IN DEGREES */
int altitude;                         /* AIRCRAFT ALTITUDE IN METERS */
float gen_fac;                        /* GENERAL SCALE FACTOR */


char head[NHEAD][10240];                  /* ARRAY OF INPUT HEADER DATA (COMPRESSED) */
signed char dat[NLINE][1024][10];       /* ARRAY OF INPUT IMAGE DATA (COMPRESSED) */





main()

{
      void newheader();
      void oldheader();

      char filename[40];                /* INPUT DATA FILE NAME */

      unsigned char out[NLINE][1024];     /* ARRAY OF OUTPUT DATA (BYTE) */
      float fout[NLINE][1024];            /* ARRAY OF OUTPUT DATA (REAL*4) */
      int rlength = 10240;              /* RECORD LENGTH OF INPUT FILE */

      float ichit;                      /* TRANSMIT ELLIPTICITY IN DEGREES */
      float ipsit;                      /* TRANSMIT ORIENTATION ANGLE IN DEGREES */
      float ichir;                      /* RECEIVE ELLIPTICITY IN DEGREES */
      float ipsir;                      /* RECEIVE ORIENTATION ANGLE IN DEGREES */
      float chit;                       /* TRANSMIT ELLIPTICITY IN RADIANS */
      float psit;                       /* TRANSMIT ORIENTATION ANGLE IN RADIANS */
      float chir;                       /* RECEIVE ELLIPTICITY IN RADIANS */
      float psir;                       /* RECEIVE ORIENTATION ANGLE IN RADIANS */
      float stvec[4];                   /* TRANSMIT STOKES VECTOR */
      float srvec[4];                   /* RECEIVE STOKES VECTOR */

      int line;                         /* LOOP INDEX THROUGH EACH LINE */
      int pixel;                        /* LOOP INDEX THROUGH EACH PIXEL */
      int count;                        /* NO. OF PIXELS USED IN CALCULATING AVG. */
      int i, j;                         /* LOOP INDICES */
      int ihead;                        /* FUNCTION VALUE FROM HEADER ROUTINES */
      float ftemp;			/* TEMPORARY REAL*4 PIXEL VALUE  */
      int itemp;                        /* TEMPORARY INTEGER PIXEL VALUE */

      float xxx[256][256];              /* DECOMPRESSION ARRAY */
      float var;                        /* INTERMEDIATE VALUE IN FOR XXX ARRAY */
      double expo;                      /* EXPONENT TO WHICH POWER IS RAISED */
      double asf;                       /* ADDITIONAL SCALE FACTOR */
      float power;                      /* POWER VALUE FOR A PARTICULAR PIXEL */
      float sumpower = 0.0;             /* POWER VALUES SUMMED OVER PIXELS */
      double gen_pow;                   /* FACTOR TO MAKE AVG. POWER EQUAL 128 */
      float multfac;                    /* FACTOR MULTIPLIED BY DECOMP. ARRAY */

      char int3, int4, int5, int6;      /* VARIABLES USED TO CALCULATE STOKES MATRIX ELEMENTS */ 
      float stokes[4][4];               /* 4 X 4 REAL SYMMETRIC STOKES MATRIX */


      int baddata_cnt = 0;              /* BAD DATA COUNT */      
      int gooddata_cnt = 0;             /* GOOD DATA COUNT */ 
      int negpower_cnt = 0;             /* NEGATIVE POWER COUNT */  
      int pospower_cnt = 0;             /* POSITIVE POWER COUNT */

      int infd;                         /* FILE DESCRIPTOR FOR INPUT DATA FILE */

/*      int xxxfd, xxxwd;               /* FILE DESCRIPTOR & CHECK VARIABLE FOR REAL*4 DECOMP. IMAGE FILE */
/*      int boutfd, boutwd;             /* FILE DESCRIPTOR & CHECK VARIABLE FOR BYTE OUTPUT IMAGE FILE */
      int outfd, outwd;                 /* FILE DESCRIPTOR & CHECK VARIABLE FOR REAL*4 OUTPUT IMAGE FILE */

/*      int stat=1;          */

      foreground();

      /* OPEN THE USER-SPECIFIED COMPRESSED DATA FILE AND WRITE IT INTO THE INPUT ARRAY "INPUT".  */

      printf("\n");
      printf("\n");
      printf("Please enter the input Compressed filename -->  ");
      scanf("%s", filename);
      printf("\n");

      infd = open(filename,O_RDONLY,0);

      if (infd > 0)
      {
         read(infd, head, NHEAD*10240);
/*	 lseek(infd,OFFSET*10240,SEEK_CUR);       */   
         read(infd, dat, NLINE*1024*10);
      }
      else
         exit(1);

/*
      stat = mpin(dat,NLINE*10*1024);
      stat = mpin(xxx,4*256*256); 
*/ 

              
      /* CHECK IF INPUT FILE HAS THE NEW HEADER FORMAT.  IF SO, READ THE PARAMETERS
         FROM THE NEW HEADER AND STORE THEM.        */

      newheader();


      /* READ THE OLD ANNOTATION RECORD AND STORE USEFUL PARAMETERS.    */

      oldheader();


/*      nline -= OFFSET;        */



      /* GET TRANSMIT AND RECEIVE POLARIZATION STATES FROM USER.  NOTE THAT
         THE ANGLES ARE EXPECTED IN DEGREES.         */

      printf("\n");
      printf("\n");
      printf("Please enter the transmit ellipticity angle in degrees -->   ");
      scanf("%f", &ichit);
      printf("\n");

      printf("Please enter the transmit orientation angle in degrees -->   ");
      scanf("%f", &ipsit);
      printf("\n");

      printf("Please enter the receive ellipticity angle in degrees -->    ");
      scanf("%f", &ichir);
      printf("\n");

      printf("Please enter the receive orientation angle in degrees -->    ");
      scanf("%f", &ipsir);
      printf("\n");
      printf("\n");


      /*  CONVERT ELLIPTICITY AND ORIENTATION ANGLES FROM DEGREES TO RADIANS.       */

      chit = ichit * PI / 180.0;
      psit = ipsit * PI / 180.0;
      chir = ichir * PI / 180.0;
      psir = ipsir * PI / 180.0;
           

      /*  CALCULATE TRANSMIT STOKES VECTORS FOR CHOSEN POLARIZATION COMBINATION.      */

      stvec[0] = 1.0;
      stvec[1] = cos( 2.0 * psit ) * cos( 2.0 * chit );
      stvec[2] = sin( 2.0 * psit ) * cos( 2.0 * chit );
      stvec[3] = sin( 2.0 * chit );


      /*  CALCULATE RECEIVE STOKES VECTORS FOR CHOSEN POLARIZATION COMBINATION.      */

      srvec[0] = 1.0;
      srvec[1] = cos( 2.0 * psir ) * cos( 2.0 * chir );
      srvec[2] = sin( 2.0 * psir ) * cos( 2.0 * chir );
      srvec[3] = sin( 2.0 * chir );


      /*  THE FOLLOWING SEGMENT OF CODE CALCULATES A DECOMPRESSION ARRAY XXX FOR
          COMPRESSED MULTIPOLARIZATION SAR DATA.  THE DECOMPRESSION ARRAY INCLUDES
          A FACTOR THAT ATTEMPTS TO SET THE AVERAGE POWER OF THE IMAGE TO A VALUE
          OF 127.0.  THEREFORE, THE RESULTING DATA SHOULD NOT BE USED FOR
          QUANTITATIVE ANALYSIS BUT RATHER FOR DISPLAY PURPOSES ONLY.                  */


      /*  AN ADDITIONAL SCALE FACTOR WHICH MAY BE SET BY USER TO CONTROL BRIGHTNESS
          OF RESULTING IMAGE.                        */

      asf = 1.0; 


      /*  AN EXPONENT TO WHICH THE POWER IS RAISED FOR DISPLAY.  NOTE THAT
          THE ('AVERAGE POWER', RAISED TO THE EXPO) IS SET EQUAL TO 127.
          SETTING THE EXPO TO 0.5 EFFECTIVELY DISPLAYS AN AMPLITUDE IMAGE,
          WHICH HAS LESS DYNAMIC RANGE THAN A POWER IMAGE AND THEREFORE LOOKS
          MORE PLEASING TO THE EYE.  IN PRACTICE AT JPL, WE OFTEN SET THE EXPO
          TO 0.25 AND DISPLAY A FOURTH ROOT IMAGE WHICH LOOKS EVEN BETTER.              */


      expo = 1.0;



      /*  COMPUTE INITIAL DECOMPRESSION ARRAY, INCLUDING GEN_FAC.                */


      for (i=0; i <= 188; ++i)
      {
         var = pow( 2.0,((double) i-128) ) * gen_fac;

         for (j=0; j <= 255; ++j)
            
            xxx[i][j] = ( (((float) j-128) / 254.0) + 1.5 ) * var;
      }



      /*  COMPUTE SUM OF TOTAL POWERS FROM SELECTED LINES OF IMAGE.            */

      power = 0.0;
      count = 0.0;

      for (line=50; line <= NLINE-50; line = line + 50)
      {
         for (pixel=0; pixel <= nsamp-1; ++pixel)
         {
            if (dat[line][pixel][1] != 0)
            {
               power = xxx[(((int) dat[line][pixel][0])+128)][(((int) dat[line][pixel][1])+128)];
               sumpower = sumpower + power;
               ++count;
            }
         }
      }




      /*  COMPUTE FACTOR TO BE PUT BACK INTO DECOMPRESSION ARRAY SO THAT
          'AVERAGE POWER' RAISED TO THE EXPO WILL COME OUT TO BE 127.
          GEN_POW = ( ASF * 127 ) / ( (AVERAGE POWER) RAISED TO THE EXPO )       */


      gen_pow = (asf * 127.0) / ( pow( (((double) sumpower)/((double) count)), expo ) );



      /*  MULTIPLY DECOMPRESSION ARRAY BY GEN_POW TO GET FINAL DECOMPRESSION ARRAY     */


      multfac = pow( gen_pow,(1 / expo) );

      for (i=0; i <= 255; ++i)
      {
         for (j=0; j <= 188; ++j) 

            xxx[j][i] = xxx[j][i] * multfac;
      }



      /*  LOOP THROUGH EACH LINE OF COMPRESSED DATA FILE           */

      for (line=0; line <= nline-1; ++line)
      {
        /* printf("%d\n",line);  */

         for (pixel=0; pixel <= nsamp-1; ++pixel)
         {
            /*  DECOMPRESS DATA TO GET STOKES MATRIX TERMS FOR THIS PIXEL. NOTE SYMMETRY.    */

	    int3 = dat[line][pixel][3];
	    int4 = dat[line][pixel][4];
	    int5 = dat[line][pixel][5];
	    int6 = dat[line][pixel][6];

            stokes[0][0] = xxx[(((int) dat[line][pixel][0])+128)][(((int) dat[line][pixel][1])+128)];
            stokes[0][1] = (dat[line][pixel][2]) * (stokes[0][0]) / 127.0;
            stokes[0][2] = STOKES(int3);
            stokes[0][3] = STOKES(int4);
            stokes[1][0] = stokes[0][1];
            stokes[1][2] = STOKES(int5);
            stokes[1][3] = STOKES(int6);
            stokes[2][0] = stokes[0][2];
            stokes[2][1] = stokes[1][2];
            stokes[2][2] = (dat[line][pixel][7]) * (stokes[0][0]) / 127.0;
            stokes[2][3] = (dat[line][pixel][8]) * (stokes[0][0]) / 127.0;
            stokes[3][0] = stokes[0][3];
            stokes[3][1] = stokes[1][3];
            stokes[3][2] = stokes[2][3];
            stokes[3][3] = (dat[line][pixel][9]) * (stokes[0][0]) / 127.0;
            stokes[1][1] = (stokes[0][0]) - (stokes[2][2]) - (stokes[3][3]);



            /*  MULTIPLY  RECEIVE STOKES VECTOR TRANSPOSED * STOKES MATRIX * TRANSMIT STOKES VECTOR
                TO GET POWER FOR THIS PIXEL.                                                             */

            power = 0.0;

            for (i=0; i <= 3; ++i)
            {
               for (j=0; j <= 3; ++j)
                  power = power + srvec[i] * stokes[i][j] * stvec[j];
            }
 


            /*  THEORETICALLY, POWER VALUE SHOULD ALWAYS BE NON-NEGATIVE FOR A
                PHYSICALLY REALIZABLE STOKES MATRIX.  BECAUSE OF QUANTIZATION AND
                COMPUTER ROUND-OFF ERROR, IT IS POSSIBLE FOR POWER VALUE TO BE ONLY
                SLIGHTLY NON-NEGATIVE.  BAD DATA COULD CAUSE THE POWER VALUE TO BE
                SIGNIFICANTLY NON-NEGATIVE.  IN EITHER CASE, THE POWER IS SET TO 0.
                AND AN INFORMATIVE MESSAGE IS DISPLAYED.                                    */

            if ( (power/(stokes[0][0])) <  -0.0001 )
            {
/*             printf("\n");                                                */                
/*             printf("Bad Data Value: %d %d %f\n",line,pixel,power);       */
               ++baddata_cnt;  
               power = 0.0;
            }
            else
               ++gooddata_cnt;  

            if ( (power/(stokes[0][0])) < 0.0 ) 
            {
/*             printf("\n");                                                */                
/*             printf("Negative Power Value: %d %d %f\n",line,pixel,power);       */
               ++negpower_cnt;  
               power = 0.0;
            }
            else
               ++pospower_cnt;


            /*  RAISE POWER VALUE TO THE EXPO AND TRUNCATE AT ZERO AND 255.      */

            ftemp = pow( ((double) power), expo );

            itemp = ROUND(ftemp);
            itemp = MIN(itemp,255);
            itemp = MAX(itemp,0);


            /*  STORE POWER VALUE IN OUTPUT ARRAY.      */

            fout[line][pixel] = ftemp;
            out[line][pixel] = itemp;

         }
      }



      /* CREATE THE OUTPUT FILES AND WRITE OUTPUT DATA IN THEM.      */


      outfd = creat("synthesize.fout",0644);
/*      boutfd = creat("synthesize.out",0644);      */
/*      xxxfd = creat("synthesize.xxx",0644);       */

      if( outfd <= 0 )
         printf("unable to create synthesize.fout file!");
      else
         printf("attempting to write %d real*4...\n",NLINE*1024*4);

/*
      if( boutfd <= 0 )
         printf("unable to create synthesize.out file!");
      else
         printf("attempting to write %d bytes...\n",NLINE*1024);
*/

      outwd = write(outfd,fout,NLINE*1024*4);
/*      boutwd = write(boutfd,out,NLINE*1024);      */ 
/*      xxxwd = write(xxxfd,xxx,256*256*4);         */

      printf("successfully wrote %d real*4 output!\n",outwd);
/*      printf("successfully wrote %d bytes output!\n",boutwd);            */


/*  
      stat = munpin(dat,NLINE*10*1024);
      stat = munpin(xxx,4*256*256); 
*/



      /*  CLOSE INPUT AND OUTPUT FILE DESCRIPTOR AND EXIT THE PROGRAM            */


      close(infd);
      close(outfd);
/*      close(boutfd);       */
/*      close(xxxfd);        */

      printf("\n");
      printf("Bad Data Count =  %d    Good Data Count =  %d",baddata_cnt,gooddata_cnt);
      printf("\n");
      printf("Negative Power Count =  %d    Positive Power Count =  %d",negpower_cnt,pospower_cnt);
      printf("\n");
      
      return 0;

}





/*

  C*******************************************************************************
  C
  C     NEWHEADER, Version 2.0
  C
  C     Howard Zebker, Lynne Norikane, Jakob van Zyl and Fred Burnette
  C     February 1990
  C
  C
  C     The variable data header exists at the beginning of the file and is
  C     composed of 20 50-byte ASCII fields.  The format of each field is as
  C     follows:
  C
  C          Byte 1                                            Byte 50
  C          DESCRIPTIVE TEXT  -- blanks (ascii 32) -- NUMERICAL VALUE
  C
  C     The descriptive text is left justified, and the numerical values are
  C     right justified.  Intermediate characters are assumed to be blanks.
  C
  C     The fields contained in the header are as follows:
  C
  C          field   contents
  C          -----   --------
  C          1       RECORD LENGTH IN BYTES =                   XXXXXXXX
  C          2       NUMBER OF HEADER RECORDS =                 XXXXXXXX
  C          3       NUMBER OF SAMPLES PER RECORD =             XXXXXXXX
  C          4       NUMBER OF LINES IN IMAGE =                 XXXXXXXX
  C          5       NUMBER OF BYTES PER SAMPLE =               XXXXXXXX
  C          6       JPL AIRCRAFT SAR PROCESSOR VERSION            XX.XX
  C          7       DATA TYPE =                       CCCCCCCCCCCCCCCCC
  C          8       RANGE PROJECTION =                           CCCCCC
  C          9       RANGE PIXEL SPACING (METERS) =            XXXX.XXXX
  C          10      AZIMUTH PIXEL SPACING (METERS) =          XXXX.XXXX
  C          11      BYTE OFFSET OF OLD HEADER =                XXXXXXXX
  C          12      BYTE OFFSET OF USER HEADER =               XXXXXXXX
  C          13      BYTE OFFSET OF FIRST DATA RECORD =         XXXXXXXX
  C          14-20   RESERVED FOR LATER USE
  C
  C     where 'X's are replaced by numerical values and 'C's are replaced
  C     by character strings.
  C
  C     If no errors occur during the execution of this routine, a value
  C     of 0 (zero) is returned.
  C
  C*******************************************************************************
*/



void newheader()
{


   /*  EXTRACT THE RECORD LENGTH PARAMETER  */

   sscanf(&head[0][42],"%d",&ireclen);


   /*  EXTRACT THE HEADER RECORD PARAMETER  */

   sscanf(&head[0][92],"%d",&nhead);


   /*  EXTRACT THE NUMBER OF SAMPLES PARAMETER  */

   sscanf(&head[0][142],"%d",&nsamp);


   /*  EXTRACT THE NUMBER OF LINES PARAMETER    */

   sscanf(&head[0][192],"%d",&nline);


   /*  EXTRACT THE # OF BYTES PER SAMPLE PARAMETER   */

   sscanf(&head[0][242],"%d",&nbytesamp);


   /*  EXTRACT THE BYTES TO OLD HEADER PARAMETER    */

   sscanf(&head[0][542],"%d",&ibytesoldhead);


   /*  EXTRACT THE BYTES TO USER HEADER PARAMETER    */

   sscanf(&head[0][592],"%d",&ibytesuserhead);


   /*  EXTRACT THE BYTES TO DATA PARAMETER      */

   sscanf(&head[0][642],"%d",&ibytesdata);


   /*  EXTRACT THE VERSION NUMBER PARAMETER    */

   sscanf(&head[0][291],"%f",&version);


   /*  EXTRACT THE RANGE SPACING PARAMETER     */

   sscanf(&head[0][441],"%f",&rspace);


   /*  EXTRACT THE AZIMUTH SPACING PARAMETER    */

   sscanf(&head[0][491],"%f",&azspace);


   /*  EXTRACT THE PROJECTION AND DATA TYPE PARAMETERS     */

   sscanf(&head[0][326],"%s",datatype1);
   sscanf(&head[0][333],"%s",datatype2);       
   sscanf(&head[0][380],"%s",projection);   


   /* PRINT THE EXTRACTED PARAMETERS   */

   printf("\n");
   printf("\n");
   printf("NEW HEADER INFORMATION\n");
   printf("\n");
   printf("No. of Header Records =  %d\n",nhead);
   printf("No. of Image Records =  %d\n",nline);
   printf("No. of Bytes per Record =  %d\n",ireclen);
   printf("No. of Bytes per Pixel =  %d\n",nbytesamp);
   printf("No. of Pixels per Record =  %d\n",nsamp);
   printf("No. of Bytes to Old Header =  %d\n",ibytesoldhead);
   printf("No. of bytes to User Header =  %d\n",ibytesuserhead);
   printf("No. of Bytes to Data =  %d\n",ibytesdata);
   printf("Version Number =  %f\n",version);
   printf("Slant Range Pixel Spacing (m)=  %f\n",rspace);
   printf("Azimuth Pixel Spacing (m)=  %f\n",azspace);
   printf("Data Type = %s %s\n",datatype1,datatype2);
   printf("Projection = %s\n",projection);     



}




/*

C*******************************************************************************
C
C     OLDHEADER, Version 2.0
C
C     Jakob van Zyl and Fred Burnette
C     February 1990
C
C     This function decodes the old header (sometimes referred to as the
C     annotation record) in the input data file.  Certain parameters are
C     saved in a common block for access by other programs.  The variable
C     'recnum' is used to determine where to look for the old header.
C     If the data is in the new format, the old header will most likely
C     be in the second record of the file.  
C
C     If no errors occur during the execution of this routine, a value
C     of 0 (zero) is returned.
C
C*******************************************************************************
*/


void oldheader()
{


   static char s[7];
   static char cb[] = "C-BAND";
   static char lb[] = "L-BAND";
   static char pb[] = "P-BAND";




   /*  EXTRACT THE NEAR RANGE PARAMETER  */

   sscanf(&head[1][75],"%f",&rangenear);


   /*  EXTRACT THE SLANT RANGE PIXEL SPACING PARAMETER  */

   sscanf(&head[1][126],"%f",&rangepix);


   /*  EXTRACT THE AZIMUTH PIXEL SPACING PARAMETER     */

   sscanf(&head[1][176],"%f",&azpix);


   /*  EXTRACT THE RADAR WAVELENGTH PARAMETER     */

   sscanf(&head[1][269],"%s",s);

   if (strcmp(s,cb) == 0) 
      wav = .0566;
   else if (strcmp(s,lb) == 0)
      wav = .24;
   else if (strcmp(s,pb) == 0)
      wav = .67;
   else
   {
      puts(" *****  UNABLE TO DECODE THE RADAR WAVELENGTH   ***** \n");
      puts("Please enter the radar wavelength in meters  --->   ");
      scanf("%f",&wav);
      printf("\n");
   }


   /*  EXTRACT THE DRIFT ANGLE PARAMETER     */

   sscanf(&head[1][723],"%f",&drift);


   /*  EXTRACT THE TRACK ANGLE PARAMETER     */

   sscanf(&head[1][972],"%f",&track);


   /*  EXTRACT THE RADAR ALTITUDE PARAMETER    */

   sscanf(&head[1][1275],"%d",&altitude);


   /*  EXTRACT THE GENERAL SCALE FACTOR PARAMETER   */

   sscanf(&head[1][6632],"%f",&gen_fac);


   /* PRINT THE EXTRACTED PARAMETERS  */

   printf("\n");
   printf("\n");
   printf("OLD HEADER INFORMATION\n");
   printf("\n");
   printf("Near Slant Range (m)=  %f\n",rangenear);
   printf("Slant Range Pixel Spacing (m)=  %f\n",rangepix);
   printf("Azimuth Pixel Spacing (m)=  %f\n",azpix);
   printf("Radar Wavelength (m)=  %f\n",wav);
   printf("Radar Altitude (m)=  %d\n",altitude);
   printf("Aircraft Track Angle (deg)=  %f\n",track);
   printf("Aircraft Drift Angle (deg)=  %f\n",drift);
   printf("General Scale Factor =  %f\n",gen_fac);



}
