#include <stdio.h>
#include <strings.h>
#include <math.h>
#include <gl/gl.h>
#include <device.h>
#include <malloc.h>
#include "/usr/local/include/forms.h"

#define xmax  640
#define ymax  485

int t_ang=20, dial_value;
FL_FORM *form_R, *form_T, *dial_func();

main(argc,argv)
int argc; char **argv;
{
 unsigned char data[xmax*ymax];
 unsigned char clt_r[208], clt_g[208], clt_b[208];
 unsigned long rgb[xmax*ymax];
 unsigned short wrred, wrgrn, wrblu;
 char *substr= NULL, num[4];
 static char *file[9999];
 static char bestimage[81];
 int nc, cnt=0, fd_clt, evnt, w_img;
 float exec_time, xzoom=1.0, yzoom=1.0;
 int i=0, j, npix=xmax, nscan=ymax, ti, tf, sz, i_start, i_stop;
 long tmp, ttob=0;

  foreground();

/* get the color table 
   fd_clt = open("/usr/people/xrhon/utils/colorg.hf",0);*/
   if( argc > 1 ) /* assume the string is the colortable */
   {
     fd_clt = open(argv[1],0);
     if( fd_clt <= 0 )
     {
	fprintf(stderr,"unable to open requested colortable: %s\n",argv[1]);
        fd_clt = open("/disk5/svs/xrhon/sar/video/color11g.hf",0);
     }
     argc--;
     argv++;
   }
   else
     fd_clt = open("/disk5/svs/xrhon/sar/video/color11g.hf",0);

   if( fd_clt <= 0 ) exit(1);
   read(fd_clt,clt_r,208);
   read(fd_clt,clt_g,208);
   read(fd_clt,clt_b,208);
   close( fd_clt );

   file[cnt] = malloc(80);

   while( (gets(file[cnt]) != NULL ) )
     file[++cnt] = malloc(80);

   fprintf(stderr,"found %d image files...\n",cnt);

   while( (substr = strstr(file[i],"hf130")) == NULL )
     i++;

   strcpy(bestimage,file[i]); /* set asside the best image */
   printf("bestimage: %s\n",bestimage);

   i_start = 0;
   i_stop = i;

  while( argc > 1 )
  {
   if( argc > 1 )
   { 
    if( argv[1][1] == 'h' )
    {
      npix = 2*npix;
      nscan = 2*nscan;
      xzoom = 2.0; yzoom = 2.0;
    }
    else if( argv[1][1] == 'i' )
      ttob = 1;
    else if( argv[1][1] == 't' )
      t_ang = atoi(&argv[1][2]);

    argc--;
    argv++;
   }
  }

   prefsize(npix,nscan);
   w_img = winopen("FlipSar: Left Mouse --forward-->  Middle Mouse <--reverse--");
   RGBmode();
   gconfig();
   wrred = wrgrn = wrblu = 0xffff;
   RGBwritemask(wrred,wrgrn,wrblu);
   cpack(0); clear();
   if( ttob ) pixmode(PM_TTOB,1);
   qdevice(LEFTMOUSE);
   qdevice(MIDDLEMOUSE);
   qdevice(RIGHTMOUSE);
   qdevice(ESCKEY);
   qdevice(F1KEY);
   qdevice(F2KEY);
   doublebuffer();
   rectzoom(xzoom,yzoom);

  i = 0;
  image_frmfile(file[i], data, clt_r, clt_g, clt_b, rgb); /* show first file */

  fprintf(stderr,"start event loop: %d  %s\n",i,file[i]);
  qreset();
  while( TRUE )
  {
    evnt = qread(&tmp);
    if( evnt == LEFTMOUSE ) /* down (left) mouse == forward */
    {  
/* discard up mouse event */
      qread(&tmp); 
      if( ++i >= cnt ) i = cnt-1; /* too far */
    }
    else if( evnt == MIDDLEMOUSE ) /* reverse */
    {
/* discard up mouse event */
      qread(&tmp);
      if( --i < 0 ) i = 0; /* too far */
    }
    else if( evnt == RIGHTMOUSE ) /* show best image */
    {
/* discard up mouse event */
      qread(&tmp);
      image_frmfile(bestimage, data, clt_r, clt_g, clt_b, rgb); /* show best */
      continue;
    }
    else if( evnt == F1KEY ) /* cycle from first to best image */
    {
/* discard up mouse event */
      qread(&tmp);
      for( i = i_start; i <= i_stop; i++ )
        image_frmfile(file[i], data, clt_r, clt_g, clt_b, rgb);

      continue;
    }
    else if( evnt == F2KEY ) /* cycle from best image to first */
    {
/* discard up mouse event */
      qread(&tmp);
      for( i = i_stop; i >= i_start; i-- )
        image_frmfile(file[i], data, clt_r, clt_g, clt_b, rgb);

      continue;
    }
    else if( evnt == ESCKEY ) /* quit */
    {
/* kill the dials */
      fl_hide_form(form_R);
      free( form_R );
      fl_hide_form(form_T);
      free( form_T );
      exit(0);
    }
    else
      continue;

/*    fprintf(stderr,"%d  %s\n",i,file[i]); */
  image_frmfile(file[i], data, clt_r, clt_g, clt_b, rgb); /* show first file */

  }
}

image_frmfile(file,data,clt_r,clt_g,clt_b,rgb)
char *file;
unsigned char *data, *clt_r, *clt_g, *clt_b;
unsigned long *rgb;
{
 char *substr, num[4];
 static int first=1;
 int j;
 int fd = open(file,0);
 int nr = read(fd,data,xmax*ymax);
 close(fd);

   for( j = 0; j < xmax*ymax; j++ )
     rgb[j] = (long) clt_r[data[j]] +(( (long) clt_g[data[j]] ) << 8) + 
		(( (long) clt_b[data[j]] ) << 16);

   lrectwrite((Screencoord)(0),(Screencoord)(0),(Screencoord)(xmax-1),
			(Screencoord)(ymax-1),rgb);
   swapbuffers();

/* set the Receive dial: */
    substr = strstr(file,"hf");
/*    nc = (int) substr - (int) strchr(substr,'.') - 3; */
    strncpy(num,(substr + 2),3);
    dial_value = atoi(num);

  if( first ) /* create the dials */
  {
    first = 0;
    form_T = dial_func(860,875,"Transmit",t_ang);
    fl_show_form(form_T,FL_PLACE_POSITION,FALSE,NULL);
    form_R = dial_func(1070,875,"Receive",dial_value);
    fl_show_form(form_R,FL_PLACE_POSITION,FALSE,NULL);
  }
  else /* set the Receive dial: */
  {
    substr = strstr(file,"hf");
    strncpy(num,(substr + 2),3);
    dial_value = atoi(num);
    reset_dial(form_R,dial_value);
  }

  return;
}
