#include "ray.h"
optics(parent_ray)
RAY *parent_ray;
{
   float unit_vec[3], refl_dir[3], cos_theta, spec, mag;
   COLOR reflection, transmission, texture;
   float w=0.0, wr=1.0, wt=0.0;
   int reflect();
   
   texture.red = 0.0;
   texture.green = 0.0;
   texture.blue = 0.0;

   transmission.red = 0.0;
   transmission.green = 0.0;
   transmission.blue = 0.0;

   reflection.red = 0.0;
   reflection.green = 0.0;
   reflection.blue = 0.0;

   if( parent_ray->strike->texture != NULL )
   {
     w = 1.0;
     texture.red = parent_ray->strike->texture->red;
     texture.green = parent_ray->strike->texture->green;
     texture.blue = parent_ray->strike->texture->blue;
     if( parent_ray->refl == NULL )
     {
       textel = parent_ray->strike->texture->index;
       parent_ray->intens.red = texture.red;
       parent_ray->intens.green = texture.green; 
       parent_ray->intens.blue = texture.blue;
       return(0);
     }
   }
   else /* put in some diffuse stuff: */
   {
     cos_theta = parent_ray->refl->dir[0] * parent_ray->strike->norm.x +
		 parent_ray->refl->dir[1] * parent_ray->strike->norm.y +
		 parent_ray->refl->dir[2] * parent_ray->strike->norm.z;
     cos_theta = 0.2 + 0.8*(1.0 - cos_theta*cos_theta);

     reflection.red = parent_ray->strike->refl->red_coeff *
			diffuse.red * cos_theta;
     reflection.green = parent_ray->strike->refl->green_coeff *
			diffuse.green * cos_theta;
     reflection.blue = parent_ray->strike->refl->blue_coeff *
   			diffuse.blue * cos_theta;
   }
/* apply the laws of reflection and refraction, using the surface info,
   incident light intensities, and some add hoc rules.
   first check pointers to see if there is anything incident on the surface */
   if( parent_ray->tran != NULL )
   {
     wt = 1.0;
     transmission.red = parent_ray->strike->tran->red_coeff *
				parent_ray->tran->intens.red;
     transmission.green = parent_ray->strike->tran->green_coeff *
				parent_ray->tran->intens.green;
     transmission.blue = parent_ray->strike->tran->blue_coeff *
				parent_ray->tran->intens.blue;
   }
/* total refl is more involved: */
   if( parent_ray->refl->strike != NULL )
   {
       w = 0.6; /* combine texture with specular reflection */
       wr = 0.4;
/* first add any specular reflection of surrounding objects */	
      reflection.red = reflection.red + 
				parent_ray->strike->refl->red_coeff *
					parent_ray->refl->intens.red;     
      reflection.green = reflection.green + 
				parent_ray->strike->refl->green_coeff *
					parent_ray->refl->intens.green;   
      reflection.blue = reflection.blue + 
				parent_ray->strike->refl->blue_coeff *
					parent_ray->refl->intens.blue;
   }    
/* if we're not in shadow, may have some 'saturated-white' specular stuff
   from the lamp, evaluate local direction to lamp, and if the reflection 
   ray is within 90 deg. add in the lamp */
   if( parent_ray->strike->shadow ) /* reduce reflection by some fract. */
   {
     reflection.red = 0.5 * reflection.red;
     reflection.green = 0.5 * reflection.green;
     reflection.blue = 0.5 * reflection.blue;
   }
   else if( NL > 0 && parent_ray->strike->refl->spec_pwr >= 0.0 )
/* specular light source reflection too... */
   {
     unit_vec[0] = lamp.x - parent_ray->strike->pos.x;
     unit_vec[1] = lamp.y - parent_ray->strike->pos.y;
     unit_vec[2] = lamp.z - parent_ray->strike->pos.z;
     mag = unit_vec[0] * unit_vec[0] +
			unit_vec[1] * unit_vec[1] +
				unit_vec[2] * unit_vec[2];
     mag = sqrt(mag);
     unit_vec[0] = unit_vec[0] / mag;
     unit_vec[1] = unit_vec[1] / mag;
     unit_vec[2] = unit_vec[2] / mag;

     cos_theta = parent_ray->refl->dir[0] * unit_vec[0] +
		 parent_ray->refl->dir[1] * unit_vec[1] +
		 parent_ray->refl->dir[2] * unit_vec[2];
/* just want the part of the curve between 0 and PI/2 */
     if( cos_theta > 0 )
     {
       spec = (float) pow(cos_theta,parent_ray->strike->refl->spec_pwr);
       reflection.red = reflection.red + spec*lamp.intens.red;
       reflection.green = reflection.green + spec*lamp.intens.green;
       reflection.blue = reflection.blue + spec*lamp.intens.blue;
      }
    } /* end if (!shadow) and specular refl. of light src. is desired */
#ifdef OUTTABLE
   if( textel == -99 )	/* if the textel has not been set yet... */
      textel = -1;
#endif
/* we're now ready to set the net intensity. assuming each of the three
   contributions is normalized to the range 0 - 255, we really need to perform
   a weighted sum. but for now, we'll use unit weights: */

   parent_ray->intens.red = w*texture.red + wr*reflection.red + wt*transmission.red;
   parent_ray->intens.green = w*texture.green + wr*reflection.green + wt*transmission.green;
   parent_ray->intens.blue = w*texture.blue + wr*reflection.blue + wt*transmission.blue;

   return(0);
}

