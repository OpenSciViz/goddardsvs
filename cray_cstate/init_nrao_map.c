#include "nrao_64.bit"

int init_nrao_map(nrao)
char nrao[512][512];
{
  int i=0, j=0, k=0, bitcnt=0, finished=0, ii, kk;
  unsigned char bits;
  char nrao_64x64[64][64];

  while( ! finished )
  {
      bits = nrao_64_bits[i];
      bits = bits << 7;
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits = bits << 6;
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits = bits << 5;
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits = bits << 4;
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits = bits << 3;
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits = bits << 2;
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits = bits << 1;
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      bits = nrao_64_bits[i];
      bits = bits >> 7;
      nrao_64x64[j][k++] = bits;

      if( k > 63 ) { k = 0; j++; }
      if( ++i > 511) finished = 1;
   }

/* cylce thru the postage stamp */
  for( i = 0, ii = 0; i < 512; i++, ii++ )
  { 
    if( ii > 63 ) ii = 0;
    for( k = 0, kk = 0; k < 512; k++, kk++ )
    {
      if( kk > 63 ) kk = 0;
      nrao[i][k] = nrao_64x64[ii][kk];
    }
  }
}
