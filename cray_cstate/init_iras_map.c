#include <stdio.h>
#include <math.h>

int init_iras_map(file,iras,n_dirbe_pix,dirbe_pointing)
char *file;
unsigned char iras[];
int n_dirbe_pix;
char dirbe_pointing[];
{
 int fd_in;
 int i, j, k;
 float uvec[3], *val, min=999999999, max= -999999999;
 float mean=0.0, rms=0.0;
 short res = 9;
 int colatt, azim, dirbe_pix;

   printf("init_iras_map> reading iras data as dirbe skymap...\n");
   fd_in = open(file,0);
   if( fd_in <= 0 )
   {
      printf("init_iras_map> unable to open: %s\n",file);
      exit(0);
   }
   val = (float *) malloc(393216*sizeof(float));
   for( i = 0; i < 393216; i++ ) 
   {
      read(fd_in,&val[i],4);
      if( val[i] > max ) max = val[i];
      if( val[i] < min ) min = val[i];
      mean = mean + val[i];
   }
   close(fd_in);
   mean = mean / (--i);
   for( i = 0; i < 393216; i++ ) 
      rms = rms + (val[i] - mean)*(val[i] - mean);
   
   rms = rms / (--i);
   rms = sqrt(rms);
   max = mean + rms/4;
   min = mean - rms/4;

   for( i = 0; i < 393216; i++ ) 
   {
      dirbe_pointing[i] = 0;
      if( val[i] > max ) val[i] = max;
      if( val[i] < min ) val[i] = min;
   }

   fd_in = open("claips:[aips.other.iras]dirbe_oneorb.pixel",0);
   if( fd_in < 0 ) printf("init_iras_map> unabele to open dirbe att...\n");
   for( i = 0; i < n_dirbe_pix; i++ )
   {
      read(fd_in,&dirbe_pix,4);
      dirbe_pointing[dirbe_pix] = 1;
   }      
/* also light up the pixels near the sun 
   {
     float sun_dir[3];
     int pix, neighb[8], n;
     short res=9;
     sun_dir[2] = 0.1825408;
     sun_dir[1] = 0.9831982;
     sun_dir[0] = -0.00001166;
     upx_pixel_number(sun_dir,&res,&pix);
     upx_8_neighbors(&pix,&res,neighb,&n);
     dirbe_pointing[pix] = 1;
     for( i=0; i < n; i++ ) dirbe_pointing[neighb[i]] = 1;
   }
*/
   for( i = 0; i < 393216; i++ )
   {
      if( dirbe_pointing[i] == 1 ) 
        iras[i] = 1 + 253 * (val[i] - min)/(max - min);
      else if( dirbe_pointing[i] == 2 )
        iras[i] = 255; 
      else
        iras[i] = 0;
   }
   free( val );

   close(fd_in);
   printf("init_iras_map> finished constructing iras map, npix= %d\n",i);
}



