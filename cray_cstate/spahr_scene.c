
#include "ray.h"
int spahr_scene()
/* a tetrahedron with the worldmap texture-map circumbscribed by a
   transluscent ball that also has the world texture map, and the usual
   three square mirrors */
{
/* make use of these globals:
global POLY *polys[N_POLYS];
global VERTEX_LIST *verts[N_VERTS];
global REFLECT *refl[N_SURFS];
global int NB, NP;
*/
int i;
TRIPLET *norm, *poly_norm();

NFXY = 0;
NCYL = 0;
NCON = 0;
NB = 0; /* 1; */
NP = 3 + 4;

/*
init_world_map("claips:[aips.other]globe.pixmap",world_map);
init_ozone_map("claips:[aips.other]ozone.dirbe",ozone_map);
*/

/* init light source postion  */
lamp.x = 300.0;
lamp.y = 700.0;
lamp.z = 1000.;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is also white, but not as bright */
diffuse.red = 64;
diffuse.green = 64;
diffuse.blue = 64;

/* a gray surface with NO specular reflection */
   refl[0] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[0]->spec_pwr = -1.0;
   refl[0]->red_coeff = 1.0;
   refl[0]->green_coeff = 1.0;
   refl[0]->blue_coeff = 1.0;
/* a gray surface with specular reflection */
   refl[1] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[1]->spec_pwr = 10.0;
   refl[1]->red_coeff = 0.5;
   refl[1]->green_coeff = 0.5;
   refl[1]->blue_coeff = 0.5;
/* a yellow surface */
   refl[2] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[2]->spec_pwr = 10.0;
   refl[2]->red_coeff = 0.9;
   refl[2]->green_coeff = 0.9;
   refl[2]->blue_coeff = 0.1;
/* a magenta surface */
   refl[3] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[3]->spec_pwr = 10.0;
   refl[3]->red_coeff = 0.9;
   refl[3]->green_coeff = 0.1;
   refl[3]->blue_coeff = 0.9;
/* a cyan surface */
   refl[4] = (REFLECT *) malloc(sizeof(REFLECT));
   refl[4]->spec_pwr = 10.0;
   refl[4]->red_coeff = 0.1;
   refl[4]->green_coeff = 0.9;
   refl[4]->blue_coeff = 0.9;

/* place the celestial sphere in the center */
   balls[0].r = 1.0;
   balls[0].id = 0;
   balls[0].thick = 0.1;
   balls[0].refl = refl[1];
   balls[0].tran = (REFRACT *) malloc(sizeof(REFRACT));
   balls[0].tran->refract_indx = 1.5;
   balls[0].tran->red_coeff = 0.5;
   balls[0].tran->green_coeff = 0.5;
   balls[0].tran->blue_coeff = 0.5;
   balls[0].texture = (TEXTURE *) malloc(sizeof(TEXTURE));
   balls[0].texture->index = -1;
   balls[0].texture->red = 0;
   balls[0].texture->green = 0;
   balls[0].texture->blue = 0;
   balls[0].x = -0.4;
   balls[0].y = -0.4;
   balls[0].z = 0.0;

/* the mirror polygons: */
   polys[0] = (POLY *) malloc(sizeof(POLY));
   polys[1] = (POLY *) malloc(sizeof(POLY));
   polys[2] = (POLY *) malloc(sizeof(POLY));
/* since none of the polygons share vertices in this scene, need to allocate
   12 distinct vertices */
   for( i=0; i<12; i++ )
   {
      verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
      coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   }

/* root polygon is bottom square: */
   polys[0]->id = 0;
   polys[0]->n_vert = 4;
   polys[0]->norm.x = 0;
   polys[0]->norm.y = 0;
   polys[0]->norm.z = 1;
   polys[0]->tran = NULL;
   polys[0]->texture = NULL;
   polys[0]->refl = refl[0];  
   polys[0]->vertex = verts[0];
   verts[0]->coord = coords[0];
   coords[0]->x = -2;
   coords[0]->y = -2;
   coords[0]->z = -1.25;
   verts[0]->next = verts[1];
   verts[1]->coord = coords[1];
   coords[1]->x = 2.0;
   coords[1]->y = -2;
   coords[1]->z = -1.25;
   verts[1]->next = verts[2];
   verts[2]->coord = coords[2];
   coords[2]->x = 2.0;
   coords[2]->y = 2.0;
   coords[2]->z = -1.25;
   verts[2]->next = verts[3];
   verts[3]->coord = coords[3];
   coords[3]->x = -2.0;
   coords[3]->y = 2.0;
   coords[3]->z = -1.25;
   verts[3]->next = verts[0];

/* next polygon: */   
   polys[1]->id = 1;
   polys[1]->n_vert = 4;
   polys[1]->norm.x = 0;
   polys[1]->norm.y = 1;
   polys[1]->norm.z = 0;
   polys[1]->tran = NULL;
   polys[1]->texture = NULL;
   polys[1]->refl = refl[0]; 
   polys[1]->vertex = verts[4];
   verts[4]->coord = coords[4];
   coords[4]->x = -1.90;
   coords[4]->y = -2.0;
   coords[4]->z = -1.15;
   verts[4]->next = verts[5];
   verts[5]->coord = coords[5];
   coords[5]->x = -1.90;
   coords[5]->y = -2.0;
   coords[5]->z = 2.0;
   verts[5]->next = verts[6];
   verts[6]->coord = coords[6];
   coords[6]->x = 2.0;
   coords[6]->y = -2.0;
   coords[6]->z = 2.0;
   verts[6]->next = verts[7];
   verts[7]->coord = coords[7];
   coords[7]->x = 2.0;
   coords[7]->y = -2.0;
   coords[7]->z = -1.15;
   verts[7]->next = verts[4];

/* next polygon: */   
   polys[2]->id = 2;
   polys[2]->n_vert = 4;
   polys[2]->norm.x = 1;
   polys[2]->norm.y = 0;
   polys[2]->norm.z = 0;
   polys[2]->tran = NULL;
   polys[2]->texture = NULL;
   polys[2]->refl = refl[0]; 
   polys[2]->vertex = verts[8];
   verts[8]->coord = coords[8];
   coords[8]->x = -2.0;
   coords[8]->y = -1.90;
   coords[8]->z = -1.15;
   verts[8]->next = verts[9];
   verts[9]->coord = coords[9];
   coords[9]->x = -2.0;
   coords[9]->y = 2.0;
   coords[9]->z = -1.15;
   verts[9]->next = verts[10];
   verts[10]->coord = coords[10];
   coords[10]->x = -2.0;
   coords[10]->y = 2.0;
   coords[10]->z = 2.0;
   verts[10]->next = verts[11];
   verts[11]->coord = coords[11];
   coords[11]->x = -2.0;
   coords[11]->y = -1.90;
   coords[11]->z = 2.0;
   verts[11]->next = verts[8];

/* the tetrahedron polygons: height of the tet is 3/2, lenght of each side
   is sqrt(3), center is located 1/3 up the height so that vectors drawn
   fomr the center to each vertex are unit length*/
   polys[3] = (POLY *) malloc(sizeof(POLY));
   polys[4] = (POLY *) malloc(sizeof(POLY));
   polys[5] = (POLY *) malloc(sizeof(POLY));
   polys[6] = (POLY *) malloc(sizeof(POLY));
/* since some of the polygons share coordinates, need to allocate
   4 distinct coordinates, */
   for( i=12; i<16; i++ )
      coords[i] = (TRIPLET *) malloc(sizeof(TRIPLET));
   for( i=12; i<24; i++ )
      verts[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));

   coords[12]->x = sqrt(3.0)/2.0 - 0.40;
   coords[12]->y = 0.0 - 0.40;
   coords[12]->z = -0.5;
 /* rotate first 120 deg in xy plane */
   coords[13]->x = sqrt(3.0)/2.0 * cos(120.0/57.3) - 0.40;
   coords[13]->y = sqrt(3.0)/2.0 * sin(120.0/57.3) - 0.40;
   coords[13]->z = -0.5;
 /* rotate first 240 deg in xy plane */
   coords[14]->x = sqrt(3.0)/2.0 * cos(240.0/57.3) - 0.40;
   coords[14]->y = sqrt(3.0)/2.0 * sin(240.0/57.3) - 0.40;
   coords[14]->z = -0.5;
/* and the 4th vertex is simply a unit length up the z axis */
   coords[15]->x = 0.0;
   coords[15]->y = 0.0;
   coords[15]->z = 1.0;

/* first triangle: */   
   polys[3]->id = 3;
   polys[3]->n_vert = 3;
   norm = poly_norm(coords[12],coords[13],coords[14]);
   polys[3]->norm.x = norm->x;
   polys[3]->norm.y = norm->y;
   polys[3]->norm.z = norm->z;
   free(norm);
   polys[3]->tran = NULL;
   polys[3]->texture = NULL;
   polys[3]->refl = refl[4]; 
   polys[3]->vertex = verts[12];
   verts[12]->coord = coords[12];
   verts[12]->next = verts[13];
   verts[13]->coord = coords[13];
   verts[13]->next = verts[14];
   verts[14]->coord = coords[14];
   verts[14]->next = verts[12];

/* 2nd triangle: */   
   polys[4]->id = 4;
   polys[4]->n_vert = 3;
   norm = poly_norm(coords[12],coords[13],coords[15]);
   polys[4]->norm.x = norm->x;
   polys[4]->norm.y = norm->y; 
   polys[4]->norm.z = norm->z;
   free(norm);
   polys[4]->tran = NULL;
   polys[4]->texture = NULL;
   polys[4]->refl = refl[3]; 
   polys[4]->vertex = verts[15];
   verts[15]->coord = coords[12];
   verts[15]->next = verts[16];
   verts[16]->coord = coords[13];
   verts[16]->next = verts[17];
   verts[17]->coord = coords[15];
   verts[17]->next = verts[15];

/* 3rd triangle: */   
   polys[5]->id = 5;
   polys[5]->n_vert = 3;
   norm = poly_norm(coords[13],coords[14],coords[15]);
   polys[4]->norm.x = norm->x;
   polys[4]->norm.y = norm->y; 
   polys[4]->norm.z = norm->z;
   free(norm);
   polys[5]->tran = NULL;
   polys[5]->texture = NULL;
   polys[5]->refl = refl[2]; 
   polys[5]->vertex = verts[18];
   verts[18]->coord = coords[13];
   verts[18]->next = verts[19];
   verts[19]->coord = coords[14];
   verts[19]->next = verts[20];
   verts[20]->coord = coords[15];
   verts[20]->next = verts[18];

/* 4rth triangle: */   
   polys[6]->id = 6;
   polys[6]->n_vert = 3;
   norm = poly_norm(coords[14],coords[12],coords[15]);
   polys[4]->norm.x = norm->x;
   polys[4]->norm.y = norm->y; 
   polys[4]->norm.z = norm->z;
   free(norm);
   polys[6]->tran = NULL;
   polys[6]->texture = NULL;
   polys[6]->refl = refl[1]; 
   polys[6]->vertex = verts[21];
   verts[21]->coord = coords[14];
   verts[21]->next = verts[22];
   verts[22]->coord = coords[12];
   verts[22]->next = verts[23];
   verts[23]->coord = coords[15];
   verts[23]->next = verts[21];

}
