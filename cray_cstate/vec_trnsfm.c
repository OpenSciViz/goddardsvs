/* apply transformation from viewing coord. to world, vtow is a global, 
allocated in cray.h */
#include "ray.h"
vec_trnsfm(v_src,v_dst)
float v_src[3], v_dst[3];
{
    v_dst[0] = vtow[0][0]*v_src[0] + vtow[0][1]*v_src[1] +
		vtow[0][2]*v_src[2] + vwpnt[0];
    v_dst[1] = vtow[1][0]*v_src[0] + vtow[1][1]*v_src[1] +
		vtow[1][2]*v_src[2] + vwpnt[1];
    v_dst[2] = vtow[2][0]*v_src[0] + vtow[2][1]*v_src[1] +
		vtow[2][2]*v_src[2] + vwpnt[2];
}

