#include "ray.h"
cobe()
{
int i;
#ifdef DEBUG
printf("cobe> COBE satellite polygons...\n");
#endif
NP = 6;
NB = 0;
NCYL = 0;
NCON = 0;
/* init light source postion  */
lamp.x = 0.0;
lamp.y = 500.0;
lamp.z = 1000.0;
/* lamp is white */
lamp.intens.red = 255;
lamp.intens.green = 255;
lamp.intens.blue = 255;
/* diffuse light is white, but not as bright */
diffuse.red = 125;
diffuse.green = 125;
diffuse.blue = 125;

/* this version of cobe is two hexagons (top & bottom) with six rectangular
   walls and three rectangles for each set of solar panels = 17 polys */
/* the sun shield will also be approximated by telve rectangles (really should
   be arcs of cylinders, with 12 trapizoids = 24 polys */
to allocate 5 polygons, sixth polygon used as a 'floor' */
for( i = 0; i < 6; i++ ) 
{
    poly[i] = (POLY *) malloc(sizeof(POLY));
    refl[i] = (REFLECT *) malloc(sizeof(REFLECT));
    poly[i]->id = i;
    poly[i]->refl = refl[i];
    poly[i]->tran = NULL;
}
/* init the surface optical properties */
refl[0]->spec_pwr = 1;
refl[0]->red_coeff = 0.9;
refl[0]->green_coeff = 0.4;
refl[0]->blue_coeff = 0.4;
refl[1]->spec_pwr = 1;
refl[1]->red_coeff = 0.4;
refl[1]->green_coeff = 0.9;
refl[1]->blue_coeff = 0.4;
refl[2]->spec_pwr = 1;
refl[2]->red_coeff = 0.4;
refl[2]->green_coeff = 0.4;
refl[2]->blue_coeff = 0.9;
refl[3]->spec_pwr = 1;
refl[3]->red_coeff = 0.9;
refl[3]->green_coeff = 0.9;
refl[3]->blue_coeff = 0.4;
refl[4]->spec_pwr = 1;
refl[4]->red_coeff = 0.9;
refl[4]->green_coeff = 0.4;
refl[4]->blue_coeff = 0.9;
refl[5]->spec_pwr = 1;
refl[5]->red_coeff = 0.4;
refl[5]->green_coeff = 0.4;
refl[5]->blue_coeff = 0.4;

/* next allocate the vertices */
for( i = 0; i < 22; i++ )
    vert[i] = (VERTEX_LIST *) malloc(sizeof(VERTEX_LIST));
for( i = 0; i < 16; i++ )
    coord[i] = (TRIPLET *) malloc(sizeof(TRIPLET));

/* put a large square underneath the satellite to collect shadows */
poly[5]->n_vert = 4;
poly[5]->norm.x = 0;
poly[5]->norm.y = 0;
poly[5]->norm.z = 1;
poly[5]->vertex = vert[18];
vert[18]->coord = coord[12];
coord[12]->x = -10;
coord[12]->y = -10;
coord[12]->z = -2;
vert[18]->next = vert[19];
vert[19]->coord = coord[13];
coord[13]->x = 10;
coord[13]->y = -10;
coord[13]->z = -2;
vert[19]->next = vert[20];
vert[20]->coord = coord[14];
coord[14]->x = 10;
coord[14]->y = 10;
coord[14]->z = -2;
vert[20]->next = vert[21];
vert[21]->coord = coord[15];
coord[15]->x = -10;
coord[15]->y = 10;
coord[15]->z = -2;
vert[21]->next = vert[18];

/* bottom 'cobe' poly is a triangle */
poly[0]->n_vert = 3;
poly[0]->norm.x = 0;
poly[0]->norm.y = 0;
poly[0]->norm.z = 1.0;
poly[0]->vertex = vert[0];
vert[0]->next = vert[1];
vert[0]->coord = coord[0];
coord[0]->x = 0.0;
coord[0]->y = 0.0;
coord[0]->z = 0.0;
vert[1]->next = vert[2];
vert[1]->coord = coord[1];
coord[1]->x = 5.0;
coord[1]->y = 0.0;
coord[1]->z = 0.0;
vert[2]->next = vert[0];
vert[2]->coord = coord[2];
coord[2]->x = 0.0;
coord[2]->y = 5.0;
coord[2]->z = 0.0;

/* top poly is a triangle */
poly[1]->n_vert = 3;
poly[1]->norm.x = 0;
poly[1]->norm.y = 0;
poly[1]->norm.z = 1.0;
poly[1]->vertex = vert[3];
vert[3]->next = vert[4];
vert[3]->coord = coord[3];
coord[3]->x = 0.0;
coord[3]->y = 0.0;
coord[3]->z = 10.0;
vert[4]->next = vert[5];
vert[4]->coord = coord[4];
coord[4]->x = 5.0;
coord[4]->y = 0.0;
coord[4]->z = 10.0;
vert[5]->next = vert[3];
vert[5]->coord = coord[5];
coord[5]->x = 0.0;
coord[5]->y = 5.0;
coord[5]->z = 10.0;

/* third polygon is solar panel in along x axis */
poly[2]->n_vert = 4;
poly[2]->norm.x = 0;
poly[2]->norm.y = 1.0;
poly[2]->norm.z = 0;
poly[2]->vertex = vert[6];
vert[6]->coord = coord[0];
vert[6]->next = vert[7];
vert[7]->coord = coord[3];
vert[7]->next = vert[8];
vert[8]->coord = coord[6];
coord[6]->x = coord[3]->x + 5;
coord[6]->y = coord[3]->y;
coord[6]->z = coord[3]->z;
vert[8]->next = vert[9];
vert[9]->coord = coord[7];
coord[7]->x = coord[6]->x;
coord[7]->y = coord[6]->y;
coord[7]->z = 0;
vert[9]->next = vert[6];

/* fourth polygon is rectangle along y axis */
poly[3]->n_vert = 4;
poly[3]->norm.x = 1.0;
poly[3]->norm.y = 0;
poly[3]->norm.z = 0;
poly[3]->vertex = vert[10];
vert[10]->coord = coord[1];
vert[10]->next = vert[11];
vert[11]->coord = coord[8];
coord[8]->x = coord[1]->x;
coord[8]->y = coord[1]->y + 5;
coord[8]->z = coord[1]->z;
vert[11]->next = vert[12];
vert[12]->coord = coord[9];
coord[9]->x = coord[8]->x;
coord[9]->y = coord[8]->y;
coord[9]->z = coord[8]->z + 10;
vert[12]->next = vert[13];
vert[13]->coord = coord[4];
vert[13]->next = vert[10];

/* fifth polygon is rectangle along -x,-y axis */
poly[4]->n_vert = 4;
poly[4]->norm.x = -cos(0.66666*3.1415926);
poly[4]->norm.y = cos(0.66666*3.1415926);
poly[4]->norm.z = 0;
poly[4]->vertex = vert[14];
vert[14]->coord = coord[2];
vert[14]->next = vert[15];
vert[15]->coord = coord[10];
coord[10]->x = coord[2]->x - 5.0*cos(0.66666*3.1415926);
coord[10]->y = coord[2]->y - 5.0*cos(0.66666*3.1415926);
coord[10]->z = coord[2]->z;
vert[15]->next = vert[16];
vert[16]->coord = coord[11];
coord[11]->x = coord[10]->x;
coord[11]->y = coord[10]->y;
coord[11]->z = coord[10]->z + 10;
vert[16]->next = vert[17];
vert[17]->coord = coord[5];
vert[17]->next = vert[14];

/* balls[0] center & size */
balls[0].id = 0;
balls[0].r = 5.0;
balls[0].thick = 0.0;
balls[0].x = 0.0;
balls[0].y = 0.0;
balls[0].z = 5.0;
balls[0].tran = NULL;
balls[0].refl = (REFLECT *) malloc(sizeof(REFLECT));
balls[0].refl->spec_pwr = 20.0;
balls[0].refl->red_coeff = 0.1;
balls[0].refl->green_coeff = 0.9;
balls[0].refl->blue_coeff = 0.1;

/* cone */
cones[0].id = 1;
cones[0].thick = 0.0;	/* solid */
cones[0].h = 10.0;
cones[0].r = 5.0;
cones[0].phase = 0.0;
cones[0].top = (TRIPLET *) malloc(sizeof(TRIPLET));
cones[0].bot = (TRIPLET *) malloc(sizeof(TRIPLET));
cones[0].axis = (TRIPLET *) malloc(sizeof(TRIPLET));
cones[0].top->x = 0.0;
cones[0].top->y = 0.0;
cones[0].top->z = 0.0;
cones[0].bot->x = 0.0;
cones[0].bot->y = 0.0;
cones[0].bot->z = 10.0;
cones[0].axis->x = (cones[0].bot->x - cones[0].top->x) / cones[0].h;
cones[0].axis->y = (cones[0].bot->y - cones[0].top->y) / cones[0].h;
cones[0].axis->z = (cones[0].bot->z - cones[0].top->z) / cones[0].h;
cones[0].refl = (REFLECT *) malloc(sizeof(REFLECT));
cones[0].refl->red_coeff = 0.1;
cones[0].refl->green_coeff = 0.1;
cones[0].refl->blue_coeff = 0.9;
cones[0].tran = NULL;

/* cylinder */
cyls[0].id = 1;
cyls[0].thick = 0.0;	/* solid */
cyls[0].h = 10.0;
cyls[0].r_sqr = 5.0 * 5.0;
cyls[0].phase = 0.0;
cyls[0].top = (TRIPLET *) malloc(sizeof(TRIPLET));
cyls[0].bot = (TRIPLET *) malloc(sizeof(TRIPLET));
cyls[0].axis = (TRIPLET *) malloc(sizeof(TRIPLET));
cyls[0].top->x = -5.0;
cyls[0].top->y = 0.0;
cyls[0].top->z = 6.0;
cyls[0].bot->x = 5.0;
cyls[0].bot->y = 0.0;
cyls[0].bot->z = 6.0;
cyls[0].axis->x = (cyls[0].top->x - cyls[0].bot->x) / cyls[0].h;
cyls[0].axis->y = (cyls[0].top->y - cyls[0].bot->y) / cyls[0].h;
cyls[0].axis->z = (cyls[0].top->z - cyls[0].bot->z) / cyls[0].h;
cyls[0].bt = cyls[0].bot->x * cyls[0].top->x + cyls[0].bot->y * cyls[0].top->y +
      		cyls[0].bot->z * cyls[0].top->z; 
cyls[0].bb = cyls[0].bot->x * cyls[0].bot->x + cyls[0].bot->y * cyls[0].bot->y +
      		cyls[0].bot->z * cyls[0].bot->z; 
cyls[0].refl = (REFLECT *) malloc(sizeof(REFLECT));
cyls[0].refl->red_coeff = 0.1;
cyls[0].refl->green_coeff = 0.1;
cyls[0].refl->blue_coeff = 0.9;
cyls[0].tran = NULL;

return(0);
}
