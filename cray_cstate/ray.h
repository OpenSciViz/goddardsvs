#include <stdio.h>
#include <math.h>
#include <malloc.h>

/*
#define DEBUG
*/
#ifdef MAIN
#define global
#else
#define global extern
#endif

/* define data structure types */

typedef struct { float red; 
		 float green; 
		 float blue; } COLOR;

typedef struct { int index;
		 float red; 
		 float green; 
		 float blue; } TEXTURE;

typedef struct { float refract_indx;
      		float red_coeff;
      		float green_coeff;
		float blue_coeff; } REFRACT;

typedef struct { float spec_pwr; 
		float red_coeff;
		float green_coeff;
		float blue_coeff; } REFLECT;

typedef struct { float x;
      		float y;
      		float z; } TRIPLET;

/* a vertex list is circular, the 'last' vertex in the list must point to
   the first vertext as its next. */
typedef struct vertex_list { TRIPLET *coord;
		struct vertex_list *next; } VERTEX_LIST;

/* a polygon is allowed to have up to 8 nearest neighbors, one in each octant.
   ie if we translate the polgon 'center' to the world origin center (without
   rotation) we can define the 8 octants to be (x,y,z): 0. (1,1,1), 1. (-1,1,1),
   2. (-1,-1,1), 3. (1,-1,1), and 4. (1,1,-1), 5. (-1,1,-1), 6. (-1,-1,-1),
   7. (1,-1,-1). in each of these octants there may be one or more polygon, if
   so we place the pointer to the nearest in the polygon data structure in each
   octant. if there are no polygons in that direction the pointer is NULL.
   this organization will presumably allow the raytracer to utilize 'coherence'
   in the scene and minimize the search time for polygon hits. */
typedef struct poly { int id; 
		 int n_vert;
      		 float thick;
		 TRIPLET norm;
	  	 REFRACT *tran;
	  	 REFLECT *refl;
      		 TEXTURE *texture;
		 VERTEX_LIST *vertex;
		 struct poly *near[8]; } POLY;

/* use a sphere as the bounding surface of an object */
typedef struct bounding_surf {	float r;
      				float x;
      				float y;
      				float z; } BOUNDING_SURF;

typedef struct { int id;
      		int n_poly;
      		BOUNDING_SURF *bs;
      		POLY *root; } POLY_OBJ;
 
typedef struct { int id;
	    	float r;
	    	float thick; /* if = 0 it is a solid ball, otherwise
0 <= thick < 1.0 represents fraction of r, distance from outer to inner surf. */
	    	float x;
	    	float y;
	    	float z;
	    	REFLECT *refl;
	    	REFRACT *tran; 
      		TEXTURE *texture; } SPHERE;

typedef struct { char *type;
      		int id;
		char shadow;
      		float thick;
		TRIPLET pos;
		TRIPLET norm;
		REFLECT *refl;
		REFRACT *tran;
      		TEXTURE *texture; } SURFACE;

typedef struct ray { unsigned char depth; 
		     COLOR intens;
          	     float base[3];
		     float dir[3];
		     SURFACE *strike;
      		     struct ray *refl;
		     struct ray *tran;
		     } RAY;

typedef struct { float x;
      	         float y; 
      	         float z;
		 float r;
		 COLOR intens; } LIGHT_SRC;

typedef struct { int id;
      		float thick;
      		float h;
      		float r;
      		float phase;  /* if surf. prop. are asymmetric */
      		TRIPLET *top;
      		TRIPLET *bot;	/* dist from top - bot <= h */
      		TRIPLET *axis;
      		REFLECT *refl;
      		REFRACT *tran; } CONE;

typedef struct { int id;
      		float thick;
      		float h;
      		float r_sqr;
      		float phase;  /* if surf. prop. are asymmetric */
      		float bt; /* dot prod. of top & bot */
      		float bb; /* dot prod. of bot & bot used in hit_cyl */
      		TRIPLET *top;
      		TRIPLET *bot;	/* dist from top - bot <= h */
      		TRIPLET *axis;
      		REFLECT *refl;
      		REFRACT *tran; } CYLINDER;

#define FALSE		0
#define TRUE		1
#define VISIBILITY	100000.0	/* meters */
#define N_SPHERES	100
#define N_CONES		2
#define N_CYLS		2
#define N_POLYS         100
#define N_SURFS         100
#define N_VERTS         100

global float rot_ang;
global float rotate[3][3];
global float *fxy_surf;
global float *fxy_slopx;
global float *fxy_slopy;
global int NSURF_ELEM;
global float KMAX;
/* for use in hit_fxy: */
global float HITEMAX;
global float HITEMIN;
global float FACETLEN;
global float FIELD_OF_VIEW;
/* if output file is meant to be a look-up table for the ivas 
#define OUTTABLE
*/
#ifdef OUTTABLE
global int ivas_fullres[1024];
#endif
global int textel;
global unsigned char texture_map[2][2]; /*[6144][3]; */
global unsigned char world_map[2][2]; /*[512][1024];*/
global unsigned char ozone_map[2][2]; /*[180][360];*/
global unsigned char sinusoid_map[2][2][2]; /*[3][256][512];*/
global unsigned char logo_map[2][2]; /*[512][512];*/
global unsigned char nrao_map[2][2]; /*[512][512]; */
global unsigned char iras_map[2]; /*[393216];*/
global unsigned char ruler_map[200][1060];
/*
global char dirbe_pointing[393216];
global int n_dirbe_pix;
*/
/* the light sources are global */
global LIGHT_SRC lamp;
global COLOR diffuse;
global float vwpnt[3];
global double vtow[3][3];

/* surface type descriptions */
global char *descr_poly;
global char *descr_ball;
global char *descr_cone;
global char *descr_cyl;
global char *descr_fxy;
global char *descr_light;
global int NL, NB, NP, NCON, NCYL, NFXY;	/* used by raystrike */
/* scene objects */
global REFLECT *refl[N_SURFS];
global POLY *polys[N_POLYS];
global TRIPLET *coords[N_VERTS];
global VERTEX_LIST *verts[N_VERTS];
global SPHERE balls[N_SPHERES];
global CONE cones[N_CONES];
global CYLINDER cyls[N_CYLS];

