      if( sky_pixel/64 < 1024 ) 
      {
        texture->red = sky_pixel / 64 / 4;
        texture->green = 0;
        texture->blue = 0;
      }
      else if( sky_pixel/64 < 2047 )
      {
        texture->red = 0;
        texture->green = sky_pixel / 64 / 4; 
        texture->blue = 0;
      }
      else if( sky_pixel/64 < 3070 )
      {
        texture->red = 0;
        texture->green = 0; 
        texture->blue = sky_pixel / 64 / 4; 
      }
      else if( sky_pixel/64 < 4093 )
      {
        texture->red = sky_pixel / 64 / 4; 
        texture->green = sky_pixel / 64 / 4; 
        texture->blue = 0;
      }
      else if( sky_pixel/64 < 5116 )
      {
        texture->red = 0;
        texture->green = sky_pixel / 64 / 4; 
        texture->blue = sky_pixel / 64 / 4; 
      }
      else
      {
        texture->red = sky_pixel / 64 / 4; 
        texture->green = 0;
        texture->blue = sky_pixel / 64 / 4; 
      }

