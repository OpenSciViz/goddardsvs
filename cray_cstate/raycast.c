#include "ray.h"
#define MAX_DEPTH 2

raycast(prev_surf,parent_ray)
SURFACE *prev_surf; /* add this cluge to help prevent successive reflections
      			from selected surface types... */
RAY *parent_ray;
{
  float refl_dir[3], trns_dir[3], eman[3];
  SURFACE *surf, *raystrike();
  int status, optics(), reflect(), transmit();
  char shadow();
  float costh;

#ifdef DEBUG
printf("raycast> from pos: %f  %f  %f\n",parent_ray->base[0],
       parent_ray->base[1],parent_ray->base[2]);
#endif
  parent_ray->strike = NULL;
  parent_ray->refl = NULL;
  parent_ray->tran = NULL;
  parent_ray->intens.red = 0.0;
  parent_ray->intens.green = 0.0;
  parent_ray->intens.blue = 0.0;

/* first check the current depth */
  if( parent_ray->depth > MAX_DEPTH ) return(0);

/* then see if this ray strikes anything */
  if( (surf = raystrike(parent_ray->base,parent_ray->dir)) == NULL )
    return(0);

/* we got something! but are we interested? */
  if( prev_surf != NULL )
  {
    if( prev_surf->type == descr_ball && surf->type == descr_ball &&
        prev_surf->id == surf->id ) /* not interested */
      return(0);
    if( prev_surf->type == descr_fxy && surf->type == descr_poly ) /* not interested */
      return(0);
  }
  if( surf->type == descr_light ) /* hit light source! say no more...*/
  {
    parent_ray->intens.red = surf->thick * lamp.intens.red;
    parent_ray->intens.green = surf->thick * lamp.intens.green;
    parent_ray->intens.blue = surf->thick * lamp.intens.blue;
    return(1);
  }
     
  parent_ray->strike = (SURFACE *) malloc(sizeof(SURFACE));
  parent_ray->strike->type = surf->type;
  parent_ray->strike->id = surf->id;
  parent_ray->strike->thick = surf->thick;
  parent_ray->strike->pos.x = surf->pos.x;
  parent_ray->strike->pos.y = surf->pos.y;
  parent_ray->strike->pos.z = surf->pos.z;
  parent_ray->strike->norm.x = surf->norm.x;
  parent_ray->strike->norm.y = surf->norm.y;
  parent_ray->strike->norm.z = surf->norm.z;
  parent_ray->strike->texture = surf->texture;
  parent_ray->strike->refl = surf->refl;
  parent_ray->strike->tran = surf->tran;

/* check shadow ray */
  if( NL > 0 )
    parent_ray->strike->shadow = shadow(&parent_ray->strike->pos);
  else
    parent_ray->strike->shadow = FALSE;

/* precess reflected ray, if there is such */
  if( parent_ray->strike->refl != NULL )
  {
    parent_ray->refl = (RAY *) malloc(sizeof(RAY));
    if( parent_ray->refl == NULL )
    {
      printf("ran out of memory!\n");
      return(0);
    }
    parent_ray->refl->depth = parent_ray->depth + 1;
    reflect(parent_ray->dir,&parent_ray->strike->norm,parent_ray->refl->dir);
    parent_ray->refl->base[0] = parent_ray->strike->pos.x;
    parent_ray->refl->base[1] = parent_ray->strike->pos.y;
    parent_ray->refl->base[2] = parent_ray->strike->pos.z;
    raycast(parent_ray->strike,parent_ray->refl);
  }
/* process refracted ray, if there is such */
  if( parent_ray->strike->tran != NULL )
  {
    parent_ray->tran = (RAY *) malloc(sizeof(RAY));
    if( parent_ray->tran == NULL )
    {
      printf("ran out of memory!\n");
      return(0);
    }
    parent_ray->tran->depth = parent_ray->depth + 1;
    status = transmit(parent_ray->strike,parent_ray->dir,
		      parent_ray->tran->dir,parent_ray->tran->base);
    raycast(parent_ray->strike,parent_ray->tran);
  }
/* if/when we get here we can evaluate this ray's color/intenisty, using a 
scheme based on the surface properties and the colors of the incident light */

  optics(parent_ray);

  free(parent_ray->strike);
  free(parent_ray->refl);
  if( parent_ray->tran != NULL)
    free(parent_ray->tran);

  return(1);
}

